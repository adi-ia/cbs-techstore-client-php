<?php

namespace edu\wisc\services\cbs\lookup\customer;

use edu\wisc\services\cbs\api\SoapService;
use edu\wisc\services\cbs\common\WssSoapHeaderBuilder;
use edu\wisc\services\cbs\lookup\customer\generated\DOIT_SOA_CUSTOMER_LOOKUP_V1_Service;
use edu\wisc\services\cbs\lookup\customer\generated\InputParameters;

/**
 * SOAP implementation of the {@link CustomerService}
 */
class SoapLookupCustomerService implements LookupCustomerService, SoapService
{
    /** URL for QA WSDL */
    const CBQA12 = 'http://pegasus.doit.wisc.edu:8018/webservices/SOAProvider/plsql/doit_soa_customer_lookup_v1/?wsdl';
    /** URL for DV WSDL */
    const CBDV12 = 'http://pegasus.doit.wisc.edu:8016/webservices/SOAProvider/plsql/doit_soa_customer_lookup_v1/?wsdl';
    /** URL for CP WSDL */
    const CBCP12 = 'http://pegasus.doit.wisc.edu:8015/webservices/SOAProvider/plsql/doit_soa_customer_lookup_v1/?wsdl';
    /** URL for PROD WSDL */
    const CBSP = 'http://galactica.doit.wisc.edu:8001/webservices/SOAProvider/plsql/doit_soa_customer_lookup_v1/?wsdl';


    /** @var \SoapClient */
    private $soapClient;

    /**
     * @inheritdoc
     */
    public function __construct($username, $password, $wsdlPath = null, \SoapClient $customerSoapClient = null)
    {
        if ($customerSoapClient !== null) {
            $this->soapClient = $customerSoapClient;
            return;
        } else if ($wsdlPath !== null) {
            $this->soapClient = new DOIT_SOA_CUSTOMER_LOOKUP_V1_Service(
                [],
                $wsdlPath
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        } else {
            $this->soapClient = new DOIT_SOA_CUSTOMER_LOOKUP_V1_Service(
                [],
                __DIR__ . '/../../../../../../resources/doit_soa_customer_lookup_v1.xml'
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        }
    }

    /**
     * @inheritdoc
     */
    public function getCustomer($customerIdentifer): LookupCustomerServiceResponse
    {
        $outputParameters = $this->soapClient->CUSTOMER_IDENTIFIER(
            new InputParameters(
                $customerIdentifer,
                ''
            )
        );
        $res = json_decode($outputParameters->getP_RESULT_MESSAGE(), true);
        return new LookupCustomerServiceResponse(
            $outputParameters->getP_STATUS(),
            $res['spvi'],
            $res['firstName'],
            $res['middleName'],
            $res['lastName'],
            $res['eligibleStores']
        );
    }
}
