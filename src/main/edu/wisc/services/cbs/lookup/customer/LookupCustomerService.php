<?php

namespace edu\wisc\services\cbs\lookup\customer;

use edu\wisc\services\cbs\api\Service;

/**
 * Service intented for looking up customers in CBS.
 */
interface LookupCustomerService extends Service
{
    /**
     * Get a customer by a customer identifier (NetId, CampusId, Customer Number)
     * @param string $customerIdentifier
     */
    public function getCustomer($customerIdentifier): LookupCustomerServiceResponse;
}
