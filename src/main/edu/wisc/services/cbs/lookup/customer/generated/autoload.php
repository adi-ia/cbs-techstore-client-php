<?php


 function autoload_eff7e68c06ebc8659a62045b3a7caac2($class)
{
    $classes = array(
        'edu\wisc\services\cbs\lookup\customer\generated\DOIT_SOA_CUSTOMER_LOOKUP_V1_Service' => __DIR__ .'/DOIT_SOA_CUSTOMER_LOOKUP_V1_Service.php',
        'edu\wisc\services\cbs\lookup\customer\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\lookup\customer\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\lookup\customer\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_eff7e68c06ebc8659a62045b3a7caac2');

// Do nothing. The rest is just leftovers from the code generation.
{
}
