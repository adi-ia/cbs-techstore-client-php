<?php

namespace edu\wisc\services\cbs\lookup\customer;

use edu\wisc\services\cbs\api\MockService;

/**
 * Mock implementation for {@link LookupCustomerService}
 */
class MockLookupCustomerService implements LookupCustomerService, MockService
{

    /** @var is response success */
    private $success;

    /**
     * @inheritdoc
     */
    public function __construct(bool $success = true)
    {
        $this->success = $success;
    }

    /**
     * @inheritdoc
     */
    public function getCustomer($customerIdentifer): LookupCustomerServiceResponse
    {
        $resultMessage = "Failed";

        if($this->success) {
            $resultMessage = "Success";
        }

        $customer = json_decode($customerString, true);

        return new LookupCustomerServiceResponse(
            $resultMessage,
            "UW00000",
            "Bucky",
            "Middle",
            "Badger",
            ["employee", "departmental"]
        );
    }
}
