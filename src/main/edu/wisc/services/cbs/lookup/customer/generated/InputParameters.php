<?php

namespace edu\wisc\services\cbs\lookup\customer\generated;

class InputParameters
{

    /**
     * @var string $P_CUSTOMER_IDENTIFIER
     */
    protected $P_CUSTOMER_IDENTIFIER = null;

    /**
     * @var string $P_TEST_MODE
     */
    protected $P_TEST_MODE = null;

    /**
     * @param string $P_CUSTOMER_IDENTIFIER
     * @param string $P_TEST_MODE
     */
    public function __construct($P_CUSTOMER_IDENTIFIER, $P_TEST_MODE)
    {
      $this->P_CUSTOMER_IDENTIFIER = $P_CUSTOMER_IDENTIFIER;
      $this->P_TEST_MODE = $P_TEST_MODE;
    }

    /**
     * @return string
     */
    public function getP_CUSTOMER_IDENTIFIER()
    {
      return $this->P_CUSTOMER_IDENTIFIER;
    }

    /**
     * @param string $P_CUSTOMER_IDENTIFIER
     * @return \edu\wisc\services\cbs\lookup\customer\generated\InputParameters
     */
    public function setP_CUSTOMER_IDENTIFIER($P_CUSTOMER_IDENTIFIER)
    {
      $this->P_CUSTOMER_IDENTIFIER = $P_CUSTOMER_IDENTIFIER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_TEST_MODE()
    {
      return $this->P_TEST_MODE;
    }

    /**
     * @param string $P_TEST_MODE
     * @return \edu\wisc\services\cbs\lookup\customer\generated\InputParameters
     */
    public function setP_TEST_MODE($P_TEST_MODE)
    {
      $this->P_TEST_MODE = $P_TEST_MODE;
      return $this;
    }

}
