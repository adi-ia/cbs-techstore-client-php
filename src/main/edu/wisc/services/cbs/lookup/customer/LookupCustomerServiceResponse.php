<?php

namespace edu\wisc\services\cbs\lookup\customer;

/**
 * Service response for {@link LookupCustomerService}
 */
class LookupCustomerServiceResponse
{
    /** @var string */
    private $status;

    /** @var string */
    private $spvi;

    /** @var string */
    private $firstName;

    /** @var string */
    private $middleName;

    /** @var string */
    private $lastName;

    /** @var array */
    private $eligibleStores;

    /**
     * Create a new Customer.
     * @param string $spvi
     * @param string $firstName
     * @param string $middleName
     * @param string $lastName
     * @param string $eligibleStores
     */
    public function __construct(
        $status,
        $spvi,
        $firstName,
        $middleName,
        $lastName,
        $eligibleStores
     ) {
        $this->status = $status;
        $this->spvi = $spvi;
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
        $this->eligibleStores = $eligibleStores;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getSpvi()
    {
        return $this->spvi;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }


    /**
     * @return string
     */
    public function getEligibleStores()
    {
        return $this->eligibleStores;
    }
}
