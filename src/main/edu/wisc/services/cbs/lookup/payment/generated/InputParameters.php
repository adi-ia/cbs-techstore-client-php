<?php

namespace edu\wisc\services\cbs\lookup\payment\generated;

class InputParameters
{

    /**
     * @var float $P_ORDER_NUMBER
     */
    protected $P_ORDER_NUMBER = null;

    /**
     * @var string $P_ORIG_SYS_DOCUMENT_REF
     */
    protected $P_ORIG_SYS_DOCUMENT_REF = null;

    /**
     * @var string $P_ORDER_NUMBER_GROUP
     */
    protected $P_ORDER_NUMBER_GROUP = null;

    /**
     * @var string $P_TEST_MODE
     */
    protected $P_TEST_MODE = null;

    /**
     * @param float $P_ORDER_NUMBER
     * @param string $P_ORIG_SYS_DOCUMENT_REF
     * @param string $P_ORDER_NUMBER_GROUP
     * @param string $P_TEST_MODE
     */
    public function __construct($P_ORDER_NUMBER, $P_ORIG_SYS_DOCUMENT_REF, $P_ORDER_NUMBER_GROUP, $P_TEST_MODE)
    {
      $this->P_ORDER_NUMBER = $P_ORDER_NUMBER;
      $this->P_ORIG_SYS_DOCUMENT_REF = $P_ORIG_SYS_DOCUMENT_REF;
      $this->P_ORDER_NUMBER_GROUP = $P_ORDER_NUMBER_GROUP;
      $this->P_TEST_MODE = $P_TEST_MODE;
    }

    /**
     * @return float
     */
    public function getP_ORDER_NUMBER()
    {
      return $this->P_ORDER_NUMBER;
    }

    /**
     * @param float $P_ORDER_NUMBER
     * @return \edu\wisc\services\cbs\lookup\payment\InputParameters
     */
    public function setP_ORDER_NUMBER($P_ORDER_NUMBER)
    {
      $this->P_ORDER_NUMBER = $P_ORDER_NUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ORIG_SYS_DOCUMENT_REF()
    {
      return $this->P_ORIG_SYS_DOCUMENT_REF;
    }

    /**
     * @param string $P_ORIG_SYS_DOCUMENT_REF
     * @return \edu\wisc\services\cbs\lookup\payment\InputParameters
     */
    public function setP_ORIG_SYS_DOCUMENT_REF($P_ORIG_SYS_DOCUMENT_REF)
    {
      $this->P_ORIG_SYS_DOCUMENT_REF = $P_ORIG_SYS_DOCUMENT_REF;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ORDER_NUMBER_GROUP()
    {
      return $this->P_ORDER_NUMBER_GROUP;
    }

    /**
     * @param string $P_ORDER_NUMBER_GROUP
     * @return \edu\wisc\services\cbs\lookup\payment\InputParameters
     */
    public function setP_ORDER_NUMBER_GROUP($P_ORDER_NUMBER_GROUP)
    {
      $this->P_ORDER_NUMBER_GROUP = $P_ORDER_NUMBER_GROUP;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_TEST_MODE()
    {
      return $this->P_TEST_MODE;
    }

    /**
     * @param string $P_TEST_MODE
     * @return \edu\wisc\services\cbs\lookup\payment\InputParameters
     */
    public function setP_TEST_MODE($P_TEST_MODE)
    {
      $this->P_TEST_MODE = $P_TEST_MODE;
      return $this;
    }

}
