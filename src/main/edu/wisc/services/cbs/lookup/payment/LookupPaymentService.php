<?php

namespace edu\wisc\services\cbs\lookup\payment;

use edu\wisc\services\cbs\api\Service;

/**
 * Service intended for looking up CBS payments.
 */
interface LookupPaymentService extends Service
{
    /**
     * Get the payments for a given order number.
     * @param string $orderNumber
     * @param string $origSysDocRef
     * @return PaymentServiceResponse
     */
    public function getOrderPayments($orderNumber, $origSysDocRef): LookupPaymentServiceResponse;
}
