<?php

namespace edu\wisc\services\cbs\lookup\payment;

/**
 * A payment on a CBS order.
 */
class Payment
{
    /** @var string */
    private $paymentMode;

    /** @var string */
    private $paymentType;

    /** @var string */
    private $total;

    /** @var string */
    private $paymentInfo;

    /** @var string */
    private $paymentNotes;

    /** @var string */
    private $paymentLineRef;

    /** @var string */
    private $register;

    /** @var string */
    private $creationDate;

    /**
     * Set variables for a payment.
     * @param string $paymentMode
     * @param string $paymentMode
     * @param string $paymentType
     * @param string $total
     * @param string $paymentInfo
     * @param string $paymentNotes
     * @param string $paymentLineRef
     * @param string $register
     * @param string $creationDate
     */
    public function __construct(
        $paymentMode,
        $paymentType,
        $total,
        $paymentInfo,
        $paymentNotes,
        $paymentLineRef,
        $register,
        $creationDate
    ) {
        $this->paymentMode = $paymentMode;
        $this->paymentType = $paymentType;
        $this->total = $total;
        $this->paymentInfo = $paymentInfo;
        $this->paymentNotes = $paymentNotes;
        $this->paymentLineRef = $paymentLineRef;
        $this->register = $register;
        $this->creationDate = $creationDate;
    }

    /**
     * @return string
     */
    public function getPaymentMode()
    {
        return $this->paymentMode;
    }

    /**
     * @param string
     * @return Payment
     */
    public function setPaymentMode($paymentMode)
    {
        $this->paymentMode = $paymentMode;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param string
     * @return Payment
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
        return $this;
    }

    /**
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param string
     * @return Payment
     */
    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentInfo()
    {
        return $this->paymentInfo;
    }

    /**
     * @param string
     * @return Payment
     */
    public function setPaymentInfo($paymentInfo)
    {
        $this->paymentInfo = $paymentInfo;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentNotes()
    {
        return $this->paymentNotes;
    }

    /**
     * @param string
     * @return Payment
     */
    public function setPaymentNotes($paymentNotes)
    {
        $this->paymentNotes = $paymentNotes;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentLineRef()
    {
        return $this->paymentLineRef;
    }

    /**
     * @param string
     * @return Payment
     */
    public function setPaymentLineRef($paymentLineRef)
    {
        $this->paymentLineRef = $paymentLineRef;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegister()
    {
        return $this->register;
    }

    /**
     * @param string
     * @return Payment
     */
    public function setRegister($register)
    {
        $this->register = $register;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param string
     * @return Payment
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
        return $this;
    }
}
