<?php

namespace edu\wisc\services\cbs\lookup\payment\generated;

class DOIT_SOA_PAYMENT_LOOKUP_V1_Service extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'InputParameters' => 'edu\\wisc\\services\\cbs\\lookup\\payment\\generated\\InputParameters',
      'OutputParameters' => 'edu\\wisc\\services\\cbs\\lookup\\payment\\generated\\OutputParameters',
      'SOAHeader' => 'edu\\wisc\\services\\cbs\\lookup\\payment\\SOAHeader',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'http://pegasus.doit.wisc.edu:8018/webservices/SOAProvider/plsql/doit_soa_payment_lookup_v1/?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param InputParameters $body
     * @return OutputParameters
     */
    public function ORDER_NUMBER(InputParameters $body)
    {
      return $this->__soapCall('ORDER_NUMBER', array($body));
    }

}
