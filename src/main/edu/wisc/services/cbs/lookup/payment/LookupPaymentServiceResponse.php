<?php

namespace edu\wisc\services\cbs\lookup\payment;

/**
 * Service response for {@link PaymentService}
 */
class LookupPaymentServiceResponse
{

    /** @var string */
    private $resultMessage;

    /** @var float */
    private $paidToDate;

    /** @var array */
    private $payments;

    /**
     * Set the payment service response.
     * @param string $resultMessage
     * @param float $paidToDate
     * @param array $payments
     */
    public function __construct(
        $resultMessage,
        $paidToDate,
        $payments
    )
    {
        $this->resultMessage = $resultMessage;
        $this->paidToDate = $paidToDate;
        $this->setPayments($payments);
    }

    /**
     * @return string
     */
    public function getResultMessage()
    {
        return $this->resultMessage;
    }

    /**
     * @return float
     */
    public function getPaidToDate()
    {
        return $this->paidToDate;
    }

    /**
     * @return array
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * Set payments, by looping through the array and creating Payment Objects.
     * @param array $payments
     * @return Payment
     */
    public function setPayments($payments)
    {
        $paymentArray = [];
        foreach($payments as $payment) {
            $paymentObj = new Payment(
                $payment['payment_mode_name'],
                $payment['payment_type_name'],
                $payment['total'],
                $payment['payment_info1'],
                $payment['payment_notes'],
                $payment['payment_line_id_ref'],
                $payment['cash_register'],
                $payment['creation_date']
            );

            array_push($paymentArray, $paymentObj);
        }

        $this->payments = $paymentArray;

        return $this;
    }
}
