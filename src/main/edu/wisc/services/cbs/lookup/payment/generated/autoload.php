<?php


 function autoload_d917809c48ceac5c99471521e4160510($class)
{
    $classes = array(
        'edu\wisc\services\cbs\lookup\payment\DOIT_SOA_PAYMENT_LOOKUP_V1_Service' => __DIR__ .'/DOIT_SOA_PAYMENT_LOOKUP_V1_Service.php',
        'edu\wisc\services\cbs\lookup\payment\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\lookup\payment\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\lookup\payment\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_d917809c48ceac5c99471521e4160510');

// Do nothing. The rest is just leftovers from the code generation.
{
}
