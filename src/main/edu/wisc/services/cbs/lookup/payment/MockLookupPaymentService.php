<?php

namespace edu\wisc\services\cbs\lookup\payment;

use edu\wisc\services\cbs\api\MockService;

/**
 * Mock implementation for {@link PaymentService}
 */
class MockLookupPaymentService implements LookupPaymentService, MockService
{

    /** @var is response success */
    private $success;

    /**
     * @inheritdoc
     */
    public function __construct(bool $success = true)
    {
        $this->success = $success;
    }

    /**
     * @inheritdoc
     */
    public function getOrderPayments($orderNumber, $origSysDocRef): LookupPaymentServiceResponse
    {
        $resultMessage = "Failed";

        if($this->success) {
            $resultMessage = "Success";
        }

        $payments = "[{\"payment_mode_name\":\"ACCT\",\"payment_type_name\":\"REQ/DOIT#\",\"total\":\"3198\",\"payment_info1\":\"D000000\",\"payment_notes\":\"Test Payment Notes\",\"payment_line_id_ref\":\"POS3333333-1\",\"cash_register\":\"Test Register\",\"creation_date\":\"13-JAN-2018\"}]";

        $paymentsArray = json_decode($payments, true);

        return new LookupPaymentServiceResponse(
            $resultMessage,
            "100",
            $paymentsArray
        );
    }
}
