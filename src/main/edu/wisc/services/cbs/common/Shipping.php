<?php

namespace edu\wisc\services\cbs\common;

/**
 * Shipping identifiers expected by CBS
 */
abstract class Shipping
{

    /** @var string CBS value for UPS shipping method */
    const UPS_SHIPPING = 'DAYTON DELIVERY';

    /** @var string CBS value for Campus Delivery shipping method */
    const CAMPUS_DELIVERY = 'CAMPUS_DELIVERY';

    /** @var string CBS value for Drop Shipping */
    const DROPSHIP = 'DROPSHIP';

    /** @var string Street address for UPS Shipping line item charge (will always be CS) */
    const UPS_ADDRESS = '1210 W. Dayton St.';

    /** @var string City for UPS Shipping line item charge (will always be CS) */
    const UPS_CITY = 'Madison';

    /** @var string State for UPS Shipping line item charge (will always be CS) */
    const UPS_STATE = 'WI';

    /** @var string Zip code for UPS Shipping line item charge (will always be CS) */
    const UPS_ZIP = '53706';

}