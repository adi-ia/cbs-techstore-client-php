<?php

namespace edu\wisc\services\cbs\common;

/**
 * Order identifiers expected by CBS
 */
abstract class Order
{

    /** @var string Fixed value for all orders of this source */
    const ORDER_SOURCE = 'DOIT MAGE';

    /** @var string Prefix used by ORIG_SYS_DOCUMENT_REF */
    const ORIG_SYS_PREFIX = 'MAGE';

    /** @var string Payment type for credit card orders */
    const CREDIT_CARD_IDENTIFIER_TYPE = 'CREDIT CARD';

    /** @var string 'Sales Rep' who sold this order (online, so Magento) */
    const SALES_REP = 'DOIT ONLINE CATALOG';

    /** @var string 'Cash Register' */
    const CASH_REGISTER = 'DOIT CATALOG';

}
