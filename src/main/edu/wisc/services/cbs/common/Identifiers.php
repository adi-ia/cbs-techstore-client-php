<?php

namespace edu\wisc\services\cbs\common;

/**
 * Person identifiers expected by CBS
 */
class Identifiers
{

    /** @var string Customer Type for 'Institutional' orders */
    const DEPARTMENTAL = 'DEPARTMENTAL';

    /** @var string Customer Type for 'Personal' orders */
    const PERSONAL = 'PERSONAL';

    /** @var string Type of identifier for 'Personal' orders */
    const PVI = 'PVI';

    /** @var string Type of identifier for 'Institutional' orders */
    const DNUMBER = 'DNUMBER';

    /** @var string Type of identifier for SOAR Parent orders */
    const SOAR_PARENT = 'SoarParent';

    /** @var string Type of identifier for Alumni orders */
    const ALUMNI = 'Alumni';

}