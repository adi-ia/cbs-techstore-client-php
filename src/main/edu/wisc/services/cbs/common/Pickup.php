<?php

namespace edu\wisc\services\cbs\common;

/**
 * Pickup identifiers expected by CBS
 */
abstract class Pickup
{

    /** @var string CBS value for Tech Store Pickup shipping method */
    const TS_PICKUP = 'TECHSTORE PICKUP';

    /** @var string CBS value for Received shipping method */
    const TS_RECEIVED = 'RECEIVED';

    /** @var string Address Line 1 for TS pickup and TS Received*/
    const TS_ADDRESS_1 = 'DoIT Tech Store at Computer Sciences';

    /** @var string Address Line 2 for TS pickup and TS Received*/
    const TS_ADDRESS_2 = '1210 West Dayton St';

    /** @var string City for TS pickup and TS Received */
    const TS_CITY = 'Madison';

    /** @var string Zip for TS pickup and TS Received */
    const TS_ZIP = '53706';

    /** @var string State for TS pickup and TS Received */
    const TS_STATE = 'WI';

    /** @var string CBS value for ECM Pickup shipping method */
    const ECM_PICKUP = 'ECM PICKUP';

    /** @var string CBS value for ECM Received shipping method */
    const ECM_RECEIVED = 'ECM RECEIVED';

    /** @var string Address Line 1 for ECM pickup and ECM Received */
    const ECM_ADDRESS_1 = 'DoIT Tech Store at East Campus Mall';

    /** @var string Address Line 2 for ECM pickup and ECM Received */
    const ECM_ADDRESS_2 = '333 East Campus Mall';

    /** @var string City for ECM pickup and ECM Received */
    const ECM_CITY = 'Madison';

    /** @var string Zip code for ECM pickup and ECM Received */
    const ECM_ZIP = '53715-1365';

    /** @var string State for ECM pickup and ECM Received */
    const ECM_STATE = 'WI';

    /** @var string CBS value for HSLC Received shipping method */
    const HSLC_RECEIVED = 'HSLC RECEIVED';

    /** @var string CBS value for HSLC Pickup shipping method */
    const HSLC_PICKUP = 'HSLC PICKUP';

    /** @var string Address Line 1 for HSLC pickup and HSLC Received */
    const HSLC_ADDRESS_1 = 'DoIT Tech Store at Health Sciences';

    /** @var string Address Line 2 for HSLC pickup and HSLC Received */
    const HSLC_ADDRESS_2 = '750 Highland Ave';

    /** @var string City for HSLC pickup and HSLC Received */
    const HSLC_CITY = 'Madison';

    /** @var string Zip for HSLC pickup and HSLC Received */
    const HSLC_ZIP = '53705-2221';

    /** @var string State for HSLC pickup and HSLC Received */
    const HSLC_STATE = 'WI';

}
