<?php

namespace edu\wisc\services\cbs\common;

/**
 * Abstract base class defining common service response functions.
 */
abstract class AbstractServiceResponse implements ServiceResponseInterface
{

    /** @var boolean */
    private $success;

    /** @var string */
    private $message;

    /**
     * ProductServiceResponse constructor.
     * @param bool $success
     * @param string $message
     */
    public function __construct($success, $message)
    {
        $this->success = $success;
        $this->message = $message;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

}