<?php

namespace edu\wisc\services\cbs\common;

/**
 * ServiceResponseInterface defines the basic contract that all services responses will adhere to.
 */
interface ServiceResponseInterface
{

    /**
     * @return bool
     */
    public function isSuccess(): bool;

    /**
     * @return string
     */
    public function getMessage(): string;

}