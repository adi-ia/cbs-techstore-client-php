<?php
namespace edu\wisc\services\cbs\product;

use edu\wisc\services\cbs\api\MockService;

class MockProductService implements ProductService, MockService
{

    /** @var ProductServiceResponse */
    private $success;

    /**
     * @inheritdoc
     */
    public function __construct(bool $success = true)
    {
        $this->success = $success;
    }


    /**
     * {@inheritDoc}
     */
    public function createProduct(Product $product): ProductServiceResponse
    {
        return new ProductServiceResponse(true, (string)$product);
    }

    /**
     * {@inheritDoc}
     */
    public function updateProduct(Product $product): ProductServiceResponse
    {
        return new ProductServiceResponse(true, (string)$product);
    }

}
