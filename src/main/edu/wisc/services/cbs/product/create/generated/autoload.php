<?php


 function autoload_1228bd3ef9528d59d14e5488088af3d4($class)
{
    $classes = array(
        'edu\wisc\services\cbs\product\create\generated\DOIT_SOA_ITEM_CREATE_V10_Service' => __DIR__ .'/DOIT_SOA_ITEM_CREATE_V10_Service.php',
        'edu\wisc\services\cbs\product\create\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\product\create\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\product\create\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_1228bd3ef9528d59d14e5488088af3d4');

// Do nothing. The rest is just leftovers from the code generation.
{
}
