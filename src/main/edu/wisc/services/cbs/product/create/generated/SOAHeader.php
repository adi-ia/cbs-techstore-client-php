<?php

namespace edu\wisc\services\cbs\product\create\generated;

class SOAHeader
{

    /**
     * @var string $Responsibility
     */
    protected $Responsibility = null;

    /**
     * @var string $RespApplication
     */
    protected $RespApplication = null;

    /**
     * @var string $SecurityGroup
     */
    protected $SecurityGroup = null;

    /**
     * @var string $NLSLanguage
     */
    protected $NLSLanguage = null;

    /**
     * @var string $Org_Id
     */
    protected $Org_Id = null;

    /**
     * @param string $Responsibility
     * @param string $RespApplication
     * @param string $SecurityGroup
     * @param string $NLSLanguage
     * @param string $Org_Id
     */
    public function __construct($Responsibility, $RespApplication, $SecurityGroup, $NLSLanguage, $Org_Id)
    {
      $this->Responsibility = $Responsibility;
      $this->RespApplication = $RespApplication;
      $this->SecurityGroup = $SecurityGroup;
      $this->NLSLanguage = $NLSLanguage;
      $this->Org_Id = $Org_Id;
    }

    /**
     * @return string
     */
    public function getResponsibility()
    {
      return $this->Responsibility;
    }

    /**
     * @param string $Responsibility
     * @return \edu\wisc\services\cbs\product\create\generated\SOAHeader
     */
    public function setResponsibility($Responsibility)
    {
      $this->Responsibility = $Responsibility;
      return $this;
    }

    /**
     * @return string
     */
    public function getRespApplication()
    {
      return $this->RespApplication;
    }

    /**
     * @param string $RespApplication
     * @return \edu\wisc\services\cbs\product\create\generated\SOAHeader
     */
    public function setRespApplication($RespApplication)
    {
      $this->RespApplication = $RespApplication;
      return $this;
    }

    /**
     * @return string
     */
    public function getSecurityGroup()
    {
      return $this->SecurityGroup;
    }

    /**
     * @param string $SecurityGroup
     * @return \edu\wisc\services\cbs\product\create\generated\SOAHeader
     */
    public function setSecurityGroup($SecurityGroup)
    {
      $this->SecurityGroup = $SecurityGroup;
      return $this;
    }

    /**
     * @return string
     */
    public function getNLSLanguage()
    {
      return $this->NLSLanguage;
    }

    /**
     * @param string $NLSLanguage
     * @return \edu\wisc\services\cbs\product\create\generated\SOAHeader
     */
    public function setNLSLanguage($NLSLanguage)
    {
      $this->NLSLanguage = $NLSLanguage;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_Id()
    {
      return $this->Org_Id;
    }

    /**
     * @param string $Org_Id
     * @return \edu\wisc\services\cbs\product\create\generated\SOAHeader
     */
    public function setOrg_Id($Org_Id)
    {
      $this->Org_Id = $Org_Id;
      return $this;
    }

}
