<?php

namespace edu\wisc\services\cbs\product\create\generated;

class DOIT_SOA_ITEM_CREATE_V10_Service extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'InputParameters' => 'edu\\wisc\\services\\cbs\\product\\create\\generated\\InputParameters',
      'OutputParameters' => 'edu\\wisc\\services\\cbs\\product\\create\\generated\\OutputParameters',
      'SOAHeader' => 'edu\\wisc\\services\\cbs\\product\\create\\generated\\SOAHeader',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'src/main/resources/doit_soa_item_create_v10.xml';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param InputParameters $body
     * @return OutputParameters
     */
    public function ITEM_CREATE(InputParameters $body)
    {
      return $this->__soapCall('ITEM_CREATE', array($body));
    }

}
