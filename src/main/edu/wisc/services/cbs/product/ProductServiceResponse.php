<?php
namespace edu\wisc\services\cbs\product;

use edu\wisc\services\cbs\common\AbstractServiceResponse;

/**
 * The status of a call to a {@link ProductService} method.
 */
class ProductServiceResponse extends AbstractServiceResponse
{
    
}
