<?php
namespace edu\wisc\services\cbs\product;

use edu\wisc\services\cbs\product\create\generated\InputParameters as CreateInputParameters;
use edu\wisc\services\cbs\product\update\generated\InputParameters as UpdateInputParameters;

/**
 * Maps a {@link Product} to an {@link InputParameters}
 */
class ProductInputParametersMapper
{
    /** @var string flag for 'create' InputParameters */
    static public $CREATE = 'create';

    /** @var string flag for 'update' InputParameters */
    static public $UPDATE = 'update';

    /**
     * Maps a {@link Product} to an {@link InputParameters} instance
     * @param Product $product
     * @param string $type "create" or "update"
     * @return create\generated\InputParameters|update\generated\InputParameters
     */
    public static function toInputParameters(Product $product, $type)
    {
        $inputParameters = null;
        if ($type === static::$CREATE) {
            $inputParameters = new CreateInputParameters(
                '', # P_ITEM_NUMBER
                '', # P_ITEM_DESCRIPTION
                '', # P_CBS_ITEM_TEMPLATE
                '', # P_ITEM_CATEGORY
                '', #P_ATTRIBUTE_SET
                '', # P_TAX_CLASS_ID
                '', # P_MANUFACTURER_NAME
                '', # P_MANUFACTURER_PART_NUMBER
                0.0, # P_COST
                0.0, # P_MINMAX_LEVEL_MIN
                0.0, # P_MINMAX_LEVEL_MAX
                '', # P_LIFE_CYCLE
                '', # P_SERIAL_CONTROL_FLAG
                '', # P_START_STRING
                '', # P_END_STRING
                0.0, # P_SERIAL_LENGTH_MIN
                0.0, # P_SERIAL_LENGTH_MAX
                '', # P_VENDOR
                '', # P_SUPPLIER_PART_NUMBER
                '', # P_UPC
                '', # P_UPC_ALTERNATE
                0.0, # P_BASE_PRICE
                '', # P_WEBSITE_PRICING
                0.0, # P_SPECIAL_DISCOUNT
                '', # P_PRICING_TEMPLATE
                '', # P_REFERENCE
                0.0, # P_COST_CENTER
                '', # P_PLANNER_CODE
                '', # P_MINMAX_ACTIVE
                '', # P_BUYER
                '', # P_PRODUCT_MANAGER
                '', # P_INVENTORY_CONTROL_TYPE
                0.0, # P_LOT_MULTIPLIER
                '', # P_SALES_POINT
                '', # P_PS_PRIMARY_LOCATOR
                '', # P_SECONDARY_LOCATOR
                0.0, # P_PS_MIN
                0.0, # P_PS_MAX
                '', # P_HSLC_PRIMARY_LOCATOR
                '', # P_HSLC_SECONDARY_LOCATOR
                0.0, # P_HSLC_MIN
                0.0, # P_HSLC_MAX
                '', # P_PRIMARY_LOCATOR
                '', # P_SECONDARY_LOCATOR
                0.0, # P_ECM_MIN
                0.0, # P_ECM_MAX
                '', # P_DEP_ELIGIBLE
                '', # P_ADD_SALES_INSTRUCTIONS
                '', # P_SALES_PROCEDURE
                '' # P_TEST_MODE
            );
        } else if ($type === static::$UPDATE) {
            $inputParameters = new UpdateInputParameters(
                '', # P_ITEM_NUMBER
                '', # P_ITEM_DESCRIPTION
                '', # P_CBS_ITEM_TEMPLATE
                '', # P_ITEM_CATEGORY
                '', # P_ATTRIBUTE_SET
                '', # P_TAX_CLASS_ID
                '', # P_MANUFACTURER_NAME
                '', # P_MANUFACTURER_PART_NUMBER
                0.0, # P_COST
                0.0, # P_MINMAX_LEVEL_MIN
                0.0, # P_MINMAX_LEVEL_MAX
                '', # P_LIFE_CYCLE
                '', # P_SERIAL_CONTROL_FLAG
                '', # P_START_STRING
                '', # P_END_STRING
                0.0, # P_SERIAL_LENGTH_MIN
                0.0, # P_SERIAL_LENGTH_MAX
                '', # P_VENDOR
                '', # P_SUPPLIER_PART_NUMBER
                '', # P_UPC
                '', # P_UPC_ALTERNATE
                0.0, # P_BASE_PRICE
                '', # P_WEBSITE_PRICING
                0.0, # P_SPECIAL_DISCOUNT
                '', # P_PRICING_TEMPLATE
                '', # P_REFERENCE
                0.0, # P_COST_CENTER
                '', # P_PLANNER_CODE
                '', # P_MINMAX_ACTIVE
                '', # P_BUYER
                '', # P_PRODUCT_MANAGER
                '', # P_INVENTORY_CONTROL_TYPE
                0.0, # P_LOT_MULTIPLIER
                '', # P_SALES_POINT
                '', # P_PS_PRIMARY_LOCATOR
                '', # P_SECONDARY_LOCATOR
                0.0, # P_PS_MIN
                0.0, # P_PS_MAX
                '', # P_HSLC_PRIMARY_LOCATOR
                '', # P_HSLC_SECONDARY_LOCATOR
                0.0, # P_HSLC_MIN
                0.0, # P_HSLC_MAX
                '', # P_PRIMARY_LOCATOR
                '', # P_SECONDARY_LOCATOR
                0.0, # P_ECM_MIN
                0.0, # P_ECM_MAX
                '', # P_DEP_ELIGIBLE
                '', # P_ADD_SALES_INSTRUCTIONS
                '', # P_SALES_PROCEDURE
                '' # P_TEST_MODE
            );
        }

        // NOTE: Keep these setters in the order of the constructor arguments above
        $inputParameters
            ->setP_ITEM_NUMBER($product->getItemNumber())
            ->setP_ITEM_DESCRIPTION($product->getDescription())
            ->setP_CBS_ITEM_TEMPLATE($product->getCbsItemTemplate())
            ->setP_ATTRIBUTE_SET($product->getAttributeSet())
            ->setP_ITEM_CATEGORY($product->getItemCategory())
            ->setP_TAX_CLASS_ID($product->getTaxClassId())
            ->setP_MANUFACTURER_NAME($product->getManufacturer())
            ->setP_MANUFACTURER_PART_NUMBER($product->getManufacturerPartNumber())
            ->setP_COST($product->getCost())
            ->setP_MINMAX_LEVEL_MIN($product->getMinQuantity())
            ->setP_MINMAX_LEVEL_MAX($product->getMaxQuantity())
            ->setP_LIFE_CYCLE($product->getLifecycle())
            ->setP_SERIAL_CONTROL_FLAG($product->getSerialControlFlag())
            ->setP_SERIAL_START_STRING($product->getSerialStartString())
            ->setP_SERIAL_END_STRING($product->getSerialEndString())
            ->setP_SERIAL_LENGTH_MIN($product->getSerialLengthMin())
            ->setP_SERIAL_LENGTH_MAX($product->getSerialLengthMax())
            ->setP_VENDOR($product->getVendor())
            ->setP_SUPPLIER_PART_NUMBER($product->getVendorPartNumber())
            ->setP_UPC($product->getUpc())
            ->setP_UPC_ALTERNATE($product->getUpcAlternate())
            ->setP_WEBSITE_PRICING($product->getWebsitePricing())
            ->setP_SPECIAL_DISCOUNT($product->getSpecialDiscount())
            ->setP_BASE_PRICE($product->getBasePrice())
            ->setP_PRICING_TEMPLATE($product->getPricingTemplate())
            ->setP_REFERENCE($product->getReference())
            ->setP_COST_CENTER($product->getCostCenter())
            ->setP_PLANNER_CODE($product->getPlannerCode())
            ->setP_MINMAX_ACTIVE($product->getMinMaxActive())
            ->setP_BUYER($product->getBuyer())
            ->setP_PRODUCT_MANAGER($product->getProductManager())
            ->setP_INVENTORY_CONTROL_TYPE($product->getInventoryControlType())
            ->setP_FIXED_LOT_MULTIPLIER($product->getFixedLotMultiplier())
            ->setP_SALES_POINT($product->getSalesPoint())
            ->setP_PS_PRIMARY_LOCATOR($product->getPsPrimaryLocator())
            ->setP_PS_SECONDARY_LOCATOR($product->getPsSecondaryLocator())
            ->setP_PS_MIN($product->getPsMin())
            ->setP_PS_MAX($product->getPsMax())
            ->setP_HSLC_PRIMARY_LOCATOR($product->getHslcPrimaryLocator())
            ->setP_HSLC_SECONDARY_LOCATOR($product->getHslcSecondaryLocator())
            ->setP_HSLC_MIN($product->getHslcMin())
            ->setP_HSLC_MAX($product->getHslcMax())
            ->setP_ECM_PRIMARY_LOCATOR($product->getEcmPrimaryLocator())
            ->setP_ECM_SECONDARY_LOCATOR($product->getEcmSecondaryLocator())
            ->setP_ECM_MIN($product->getEcmMin())
            ->setP_ECM_MAX($product->getEcmMax())
            ->setP_DEP_ELIGIBLE($product->getDepEligible())
            ->setP_ADD_SALES_INSTRUCTIONS($product->getAddSalesInstructions())
            ->setP_SALES_PROCEDURE($product->getSalesProcedure())
            ->setP_TEST_MODE($product->getTestMode())
            ;

        return $inputParameters;
    }

}
