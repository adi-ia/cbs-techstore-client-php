<?php


 function autoload_ed210ddcf1cf52af1d4df1bc199e496c($class)
{
    $classes = array(
        'edu\wisc\services\cbs\product\update\generated\DOIT_SOA_ITEM_UPDATE_V10_Service' => __DIR__ .'/DOIT_SOA_ITEM_UPDATE_V10_Service.php',
        'edu\wisc\services\cbs\product\update\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\product\update\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\product\update\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_ed210ddcf1cf52af1d4df1bc199e496c');

// Do nothing. The rest is just leftovers from the code generation.
{
}
