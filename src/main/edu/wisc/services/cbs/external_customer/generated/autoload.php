<?php


 function autoload_94342b6b80aaf535eda139019c2418ae($class)
{
    $classes = array(
        'edu\wisc\services\cbs\external_customer\generated\DOIT_SOA_EXTERNAL_CUSTOMER_V3_Service' => __DIR__ .'/DOIT_SOA_EXTERNAL_CUSTOMER_V3_Service.php',
        'edu\wisc\services\cbs\external_customer\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\external_customer\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\external_customer\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_94342b6b80aaf535eda139019c2418ae');

// Do nothing. The rest is just leftovers from the code generation.
{
}
