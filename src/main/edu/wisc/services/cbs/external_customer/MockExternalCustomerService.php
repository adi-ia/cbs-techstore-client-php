<?php

namespace edu\wisc\services\cbs\external_customer;

/**
 * Mock implementation of {@link ExternalCustomerService}
 */
class MockExternalCustomerService implements ExternalCustomerService
{

    /** @var ExternalCustomerServiceResponse */
    private $response;

    /**
     * MockExternalCustomerService constructor.
     * @param ExternalCustomerServiceResponse|null $response
     */
    public function __construct(ExternalCustomerServiceResponse $response = null)
    {
        if ($response !== null) {
            $this->response = $response;
        } else {
            $this->response = new ExternalCustomerServiceResponse(
                true,
                'MockExternalCustomerServiceResponse',
                '123456789'
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createCustomer(ExternalCustomer $customer)
    {
        return $this->response;
    }

}