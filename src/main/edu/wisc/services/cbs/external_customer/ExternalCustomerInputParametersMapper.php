<?php

namespace edu\wisc\services\cbs\external_customer;

use edu\wisc\services\cbs\external_customer\generated\InputParameters;

/**
 * Maps an {@link ExternalCustomer} to an {@link InputParameters}
 */
class ExternalCustomerInputParametersMapper
{

    /**
     * Maps an {@link ExternalCustomer} to {@link InputParameters}
     * @param ExternalCustomer $customer
     * @return InputParameters
     */
    public static function toInputParameters(ExternalCustomer $customer)
    {
        return (new InputParameters(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ))
            ->setP_CUSTOMER_TYPE($customer->getCustomerType())
            ->setP_CUSTOMER_IDENTIFIER($customer->getCustomerIdentifier())
            ->setP_FIRST_NAME($customer->getFirstName())
            ->setP_LAST_NAME($customer->getLastName())
            ->setP_EMAIL($customer->getEmail())
            ->setP_PHONE($customer->getPhone())
            ->setP_REFERENCE($customer->getReference())
            ->setP_STUDENT_FIRST_NAME($customer->getStudentFirstName())
            ->setP_STUDENT_LAST_NAME($customer->getStudentLastName())
            ->setP_TEST_MODE($customer->getTestMode());
    }

}