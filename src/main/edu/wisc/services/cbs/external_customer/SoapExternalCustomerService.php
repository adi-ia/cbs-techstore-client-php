<?php

namespace edu\wisc\services\cbs\external_customer;

use edu\wisc\services\cbs\api\SoapService;
use edu\wisc\services\cbs\common\WssSoapHeaderBuilder;
use edu\wisc\services\cbs\external_customer\generated\DOIT_SOA_EXTERNAL_CUSTOMER_V3_Service;

/**
 * SOAP backed implementation of {@link ExternalCustomerService}
 */
class SoapExternalCustomerService implements SoapService, ExternalCustomerService
{

    /** URL for QA WSDL */
    const CBQA12 = 'http://pegasus.doit.wisc.edu:8018/webservices/SOAProvider/plsql/doit_soa_external_customer_v3/?wsdl';
    /** URL for DV WSDL */
    const CBDV12 = 'http://pegasus.doit.wisc.edu:8016/webservices/SOAProvider/plsql/doit_soa_external_customer_v3/?wsdl';
    /** URL for CP WSDL */
    const CBCP12 = 'http://pegasus.doit.wisc.edu:8015/webservices/SOAProvider/plsql/doit_soa_external_customer_v3/?wsdl';
    /** URL for PROD WSDL */
    const CBSP = 'http://galactica.doit.wisc.edu:8001/webservices/SOAProvider/plsql/doit_soa_external_customer_v3/?wsdl';

    /** @var DOIT_SOA_EXTERNAL_CUSTOMER_V3_Service */
    private $soapClient;

    /**
     * {@inheritdoc}
     */
    public function __construct($username, $password, $wsdlPath = null, \SoapClient $externalCustomerSoapClient = null)
    {
        if ($externalCustomerSoapClient !== null) {
            $this->soapClient = $externalCustomerSoapClient;
            return;
        } else if ($wsdlPath !== null) {
            $this->soapClient = new DOIT_SOA_EXTERNAL_CUSTOMER_V3_Service(
                [],
                $wsdlPath
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        } else {
            $this->soapClient = new DOIT_SOA_EXTERNAL_CUSTOMER_V3_Service(
                [],
                __DIR__ . '/../../../../../resources/doit_soa_external_customer_v3.xml'
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createCustomer(ExternalCustomer $customer)
    {
        $outputParameters = $this->soapClient->CREATE_CUSTOMER(
            ExternalCustomerInputParametersMapper::toInputParameters($customer)
        );
        return new ExternalCustomerServiceResponse(
            strcasecmp($outputParameters->getP_STATUS(), 'SUCCESS') === 0,
            $outputParameters->getP_RESULT_MESSAGE(),
            $outputParameters->getP_CUSTOMER_NUMBER()
        );
    }

}