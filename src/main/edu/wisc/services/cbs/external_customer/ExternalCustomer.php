<?php

namespace edu\wisc\services\cbs\external_customer;

/**
 * ExternalCustomer represents an external customer, for example, a Parent
 */
class ExternalCustomer
{

    /** @var string */
    private $customerType;

    /** @var string */
    private $customerIdentifier;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $email;

    /** @var string */
    private $phone;

    /** @var string */
    private $studentFirstName;

    /** @var string */
    private $studentLastName;

    /** @var string */
    private $reference;

    /** @var string */
    private $testMode;

    /**
     * @return string
     */
    public function getCustomerType()
    {
        return $this->customerType;
    }

    /**
     * @param string $customerType
     * @return ExternalCustomer
     */
    public function setCustomerType($customerType)
    {
        $this->customerType = $customerType;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerIdentifier()
    {
        return $this->customerIdentifier;
    }

    /**
     * @param string $customerIdentifier
     * @return ExternalCustomer
     */
    public function setCustomerIdentifier($customerIdentifier)
    {
        $this->customerIdentifier = $customerIdentifier;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return ExternalCustomer
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return ExternalCustomer
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return ExternalCustomer
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return ExternalCustomer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return ExternalCustomer
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return string
     */
    public function getStudentFirstName()
    {
        return $this->studentFirstName;
    }

    /**
     * @param string $studentFirstName
     * @return ExternalCustomer
     */
    public function setStudentFirstName(string $studentFirstName)
    {
        $this->studentFirstName = $studentFirstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getStudentLastName()
    {
        return $this->studentLastName;
    }

    /**
     * @param string $studentLastName
     * @return ExternalCustomer
     */
    public function setStudentLastName(string $studentLastName)
    {
        $this->studentLastName = $studentLastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTestMode()
    {
        return $this->testMode;
    }

    /**
     * @param string $testMode
     * @return ExternalCustomer
     */
    public function setTestMode($testMode)
    {
        $this->testMode = $testMode;
        return $this;
    }

}