<?php

namespace edu\wisc\services\cbs\item_comp;

use edu\wisc\services\cbs\api\SoapService;
use edu\wisc\services\cbs\common\WssSoapHeaderBuilder;
use edu\wisc\services\cbs\item_comp\generated\DOIT_SOA_ITEM_COMP_V1_Service;
use edu\wisc\services\cbs\item_comp\generated\InputParameters;
use edu\wisc\services\cbs\item_comp\generated\OutputParameters;

/**
 * SOAP Implementation of {@link ItemCompareService}.
 */
class SoapItemCompareService implements ItemCompareService, SoapService
{

    /** URL for QA WSLD */
    const CBQA12 = 'http://pegasus.doit.wisc.edu:8018/webservices/SOAProvider/plsql/doit_soa_item_comp_v1/?wsdl';
    /** URL for DV WSDL */
    const CBDV12 = 'http://pegasus.doit.wisc.edu:8016/webservices/SOAProvider/plsql/doit_soa_item_comp_v1/?wsdl';
    /** URL for CP WSDL */
    const CBCP12 = 'http://pegasus.doit.wisc.edu:8015/webservices/SOAProvider/plsql/doit_soa_item_comp_v1/?wsdl';
    /** URL for PROD WSDL */
    const CBSP = 'http://galactica.doit.wisc.edu:8001/webservices/SOAProvider/plsql/doit_soa_item_comp_v1/?wsdl';

    /** @var DOIT_SOA_ITEM_COMP_V1_Service */
    private $soapClient;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        $username,
        $password,
        $wsdlPath = null,
        \SoapClient $soapClient = null
    ) {
        if ($soapClient !== null) {
            $this->soapClient = $soapClient;
        } else if ($wsdlPath !== null) {
            $this->soapClient = new DOIT_SOA_ITEM_COMP_V1_Service(
                [],
                $wsdlPath
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        } else {
            $this->soapClient = new DOIT_SOA_ITEM_COMP_V1_Service(
                [],
                __DIR__ . '/../../../../../resources/doit_soa_item_comp_v1.xml'
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function compareItemWithCbs($sku)
    {
        /** @var InputParameters $inputParameters */
        $inputParameters = new InputParameters($sku, 0.0);
        /** @var OutputParameters $response */
        $outputParameters = $this->soapClient->COMPARE_CBS_MAGE_ITEM($inputParameters);

        return new ItemCompareServiceResponse($outputParameters->getP_RESULT_STRING());
    }

}
