<?php

namespace edu\wisc\services\cbs\item_comp;

/**
 * Service Response for {@link ItemCompareService}.
 */
class ItemCompareServiceResponse
{

    /** @var string */
    private $cbsResult;

    public function __construct(string $cbsResult)
    {
        $this->cbsResult = $cbsResult;
    }

    /**
     * @return string
     */
    public function getCbsResult()
    {
        return $this->cbsResult;
    }

}