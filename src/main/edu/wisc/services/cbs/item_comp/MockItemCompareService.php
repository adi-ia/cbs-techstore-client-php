<?php

namespace edu\wisc\services\cbs\item_comp;

use edu\wisc\services\cbs\api\MockService;
use edu\wisc\services\cbs\common\ServiceResponseInterface;

/**
 * Mock implementation of {@link ItemCompareService}.
 */
class MockItemCompareService implements ItemCompareService
{

    private $response;

    public function __construct(ServiceResponseInterface $response = null)
    {
        if ($response !== null) {
            $this->response = $response;
        } else {
            $this->response = new ItemCompareServiceResponse(
                '{"SKU": "12345", "attributes": [{"attribute_code":"ITEM_CATEGORY","cbs_value":"HARD/MP3Players/EARBUDS","mage_value":"SERV/PUB"}]}'
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function compareItemWithCbs($sku)
    {
        return $this->response;
    }

}