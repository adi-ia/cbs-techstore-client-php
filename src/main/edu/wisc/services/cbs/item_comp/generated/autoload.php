<?php


 function autoload_8bedcd30ef75bb86b1c1e5ef7faa06d7($class)
{
    $classes = array(
        'edu\wisc\services\cbs\item_comp\generated\DOIT_SOA_ITEM_COMP_V1_Service' => __DIR__ .'/DOIT_SOA_ITEM_COMP_V1_Service.php',
        'edu\wisc\services\cbs\item_comp\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\item_comp\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\item_comp\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_8bedcd30ef75bb86b1c1e5ef7faa06d7');

// Do nothing. The rest is just leftovers from the code generation.
{
}
