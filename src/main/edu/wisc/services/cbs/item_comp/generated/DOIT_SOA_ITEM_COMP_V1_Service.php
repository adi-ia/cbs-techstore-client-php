<?php

namespace edu\wisc\services\cbs\item_comp\generated;

class DOIT_SOA_ITEM_COMP_V1_Service extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'InputParameters' => 'edu\\wisc\\services\\cbs\\item_comp\\generated\\InputParameters',
      'OutputParameters' => 'edu\\wisc\\services\\cbs\\item_comp\\generated\\OutputParameters',
      'SOAHeader' => 'edu\\wisc\\services\\cbs\\item_comp\\generated\\SOAHeader',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'src/main/resources/doit_soa_item_comp_v1.xml';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param InputParameters $body
     * @return OutputParameters
     */
    public function COMPARE_CBS_MAGE_ITEM(InputParameters $body)
    {
      return $this->__soapCall('COMPARE_CBS_MAGE_ITEM', array($body));
    }

}
