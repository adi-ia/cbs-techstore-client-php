<?php

namespace edu\wisc\services\cbs\order\header;

use edu\wisc\services\cbs\common\Order;
use edu\wisc\services\cbs\order\payment\OrderPayment;

/**
 * Intermediate representation of an "order header" for CBS.
 */
class OrderHeader extends Order
{

    /** @var float order number from Magento */
    private $orderNumber;

    /** @var string Unique identifier, format "MAGE" + Magento order number */
    private $origSystemDocumentRef;

    /** @var string Personal/Institutional */
    private $customerType;

    /** @var string DoIT Number or PVI (student PVI if SoarParent identifier type) */
    private $customerIdentifier;

    /**
     * @var string Type of identifier being used for customer
     * @see Order::INSTITUTIONAL_IDENTIFIER_TYPE
     * @see Order::PERSONAL_IDENTIFIER_TYPE
     * @see Order::SOAR_PARENT_IDENTIFIER_TYPE
     */
    private $customerIdentifierType;

    /** @var string DoIT Number or PVI (student PVI if SoarParent identifier type) */
    private $purchaserIdentifier;

    /**
     * @var string Type of identifier being used for customer
     * @see Order::INSTITUTIONAL_IDENTIFIER_TYPE
     * @see Order::PERSONAL_IDENTIFIER_TYPE
     * @see Order::SOAR_PARENT_IDENTIFIER_TYPE
     */
    private $purchaserIdentifierType;

    /** @var string Magento customer account ID */
    private $customerReferenceField;

    /** @var string Fixed value for all orders of this source */
    private $orderSource = Order::ORDER_SOURCE;

    /**
     * @var string Payment type
     * @see OrderPayment#getPaymentType()
     */
    private $interfaceTypeFlag;

    /** @var string */
    private $defaultSet;

    /** @var string */
    private $testMode;

    /**
     * @return float
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param float $orderNumber
     * @return OrderHeader
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrigSystemDocumentRef()
    {
        return $this->origSystemDocumentRef;
    }

    /**
     * @param string $origSystemDocumentRef
     * @return OrderHeader
     */
    public function setOrigSystemDocumentRef($origSystemDocumentRef)
    {
        $this->origSystemDocumentRef = $origSystemDocumentRef;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerType()
    {
        return $this->customerType;
    }

    /**
     * @param string $customerType
     * @return OrderHeader
     */
    public function setCustomerType($customerType)
    {
        $this->customerType = $customerType;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerIdentifier()
    {
        return $this->customerIdentifier;
    }

    /**
     * @param string $customerIdentifier
     * @return OrderHeader
     */
    public function setCustomerIdentifier($customerIdentifier)
    {
        $this->customerIdentifier = $customerIdentifier;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerIdentifierType()
    {
        return $this->customerIdentifierType;
    }

    /**
     * @param string $customerIdentifierType
     * @return OrderHeader
     */
    public function setCustomerIdentifierType($customerIdentifierType)
    {
        $this->customerIdentifierType = $customerIdentifierType;
        return $this;
    }

    /**
     * @return string
     */
    public function getPurchaserIdentifier()
    {
        return $this->purchaserIdentifier;
    }

    /**
     * @param string $purchaserIdentifier
     * @return OrderHeader
     */
    public function setPurchaserIdentifier($purchaserIdentifier)
    {
        $this->purchaserIdentifier = $purchaserIdentifier;
        return $this;
    }

    /**
     * @return string
     */
    public function getPurchaserIdentifierType()
    {
        return $this->purchaserIdentifierType;
    }

    /**
     * @param string $purchaserIdentifierType
     * @return OrderHeader
     */
    public function setPurchaserIdentifierType($purchaserIdentifierType)
    {
        $this->purchaserIdentifierType = $purchaserIdentifierType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomerReferenceField()
    {
        return $this->customerReferenceField;
    }

    /**
     * @param string|null $customerReferenceField
     * @return OrderHeader
     */
    public function setCustomerReferenceField($customerReferenceField)
    {
        $this->customerReferenceField = $customerReferenceField;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderSource()
    {
        return $this->orderSource;
    }

    /**
     * @param string $orderSource
     * @return OrderHeader
     */
    public function setOrderSource($orderSource)
    {
        $this->orderSource = $orderSource;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInterfaceTypeFlag()
    {
        return $this->interfaceTypeFlag;
    }

    /**
     * @param string $interfaceTypeFlag
     * @return OrderHeader
     */
    public function setInterfaceTypeFlag($interfaceTypeFlag)
    {
        $this->interfaceTypeFlag = $interfaceTypeFlag;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDefaultSet()
    {
        return $this->defaultSet;
    }

    /**
     * @param string $defaultSet
     * @return OrderHeader
     */
    public function setDefaultSet($defaultSet)
    {
        $this->defaultSet = $defaultSet;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTestMode()
    {
        return $this->testMode;
    }

    /**
     * @param string $testMode
     * @return OrderHeader
     */
    public function setTestMode($testMode)
    {
        $this->testMode = $testMode;
        return $this;
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function __toString()
    {
        $reflect = new \ReflectionClass($this);
        $str = '';

        foreach ($reflect->getProperties() as $prop) {
            $prop->setAccessible(true);
            $str .= $prop->getName() . ': ' . $prop->getValue($this) . ' ';
        }

        return $str;
    }

}