<?php

namespace edu\wisc\services\cbs\order\header\generated;

class InputParameters
{

    /**
     * @var float $P_ORDER_NUMBER
     */
    protected $P_ORDER_NUMBER = null;

    /**
     * @var string $P_ORIG_SYS_DOCUMENT_REF
     */
    protected $P_ORIG_SYS_DOCUMENT_REF = null;

    /**
     * @var string $P_CUSTOMER_TYPE
     */
    protected $P_CUSTOMER_TYPE = null;

    /**
     * @var string $P_CUSTOMER_IDENTIFIER
     */
    protected $P_CUSTOMER_IDENTIFIER = null;

    /**
     * @var string $P_CUSTOMER_IDENTIFIER_TYPE
     */
    protected $P_CUSTOMER_IDENTIFIER_TYPE = null;

    /**
     * @var string $P_PURCHASER_IDENTIFIER
     */
    protected $P_PURCHASER_IDENTIFIER = null;

    /**
     * @var string $P_PURCHASER_IDENTIFIER_TYPE
     */
    protected $P_PURCHASER_IDENTIFIER_TYPE = null;

    /**
     * @var string $P_CUSTOMER_REFERENCE_FIELD
     */
    protected $P_CUSTOMER_REFERENCE_FIELD = null;

    /**
     * @var string $P_ORDER_SOURCE
     */
    protected $P_ORDER_SOURCE = null;

    /**
     * @var string $P_INTERFACE_TYPE_FLAG
     */
    protected $P_INTERFACE_TYPE_FLAG = null;

    /**
     * @var string $P_DEFAULT_SET
     */
    protected $P_DEFAULT_SET = null;

    /**
     * @var string $P_TEST_MODE
     */
    protected $P_TEST_MODE = null;

    /**
     * @param float $P_ORDER_NUMBER
     * @param string $P_ORIG_SYS_DOCUMENT_REF
     * @param string $P_CUSTOMER_TYPE
     * @param string $P_CUSTOMER_IDENTIFIER
     * @param string $P_CUSTOMER_IDENTIFIER_TYPE
     * @param string $P_PURCHASER_IDENTIFIER
     * @param string $P_PURCHASER_IDENTIFIER_TYPE
     * @param string $P_CUSTOMER_REFERENCE_FIELD
     * @param string $P_ORDER_SOURCE
     * @param string $P_INTERFACE_TYPE_FLAG
     * @param string $P_DEFAULT_SET
     * @param string $P_TEST_MODE
     */
    public function __construct($P_ORDER_NUMBER, $P_ORIG_SYS_DOCUMENT_REF, $P_CUSTOMER_TYPE, $P_CUSTOMER_IDENTIFIER, $P_CUSTOMER_IDENTIFIER_TYPE, $P_PURCHASER_IDENTIFIER, $P_PURCHASER_IDENTIFIER_TYPE, $P_CUSTOMER_REFERENCE_FIELD, $P_ORDER_SOURCE, $P_INTERFACE_TYPE_FLAG, $P_DEFAULT_SET, $P_TEST_MODE)
    {
      $this->P_ORDER_NUMBER = $P_ORDER_NUMBER;
      $this->P_ORIG_SYS_DOCUMENT_REF = $P_ORIG_SYS_DOCUMENT_REF;
      $this->P_CUSTOMER_TYPE = $P_CUSTOMER_TYPE;
      $this->P_CUSTOMER_IDENTIFIER = $P_CUSTOMER_IDENTIFIER;
      $this->P_CUSTOMER_IDENTIFIER_TYPE = $P_CUSTOMER_IDENTIFIER_TYPE;
      $this->P_PURCHASER_IDENTIFIER = $P_PURCHASER_IDENTIFIER;
      $this->P_PURCHASER_IDENTIFIER_TYPE = $P_PURCHASER_IDENTIFIER_TYPE;
      $this->P_CUSTOMER_REFERENCE_FIELD = $P_CUSTOMER_REFERENCE_FIELD;
      $this->P_ORDER_SOURCE = $P_ORDER_SOURCE;
      $this->P_INTERFACE_TYPE_FLAG = $P_INTERFACE_TYPE_FLAG;
      $this->P_DEFAULT_SET = $P_DEFAULT_SET;
      $this->P_TEST_MODE = $P_TEST_MODE;
    }

    /**
     * @return float
     */
    public function getP_ORDER_NUMBER()
    {
      return $this->P_ORDER_NUMBER;
    }

    /**
     * @param float $P_ORDER_NUMBER
     * @return \edu\wisc\services\cbs\order\header\generated\InputParameters
     */
    public function setP_ORDER_NUMBER($P_ORDER_NUMBER)
    {
      $this->P_ORDER_NUMBER = $P_ORDER_NUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ORIG_SYS_DOCUMENT_REF()
    {
      return $this->P_ORIG_SYS_DOCUMENT_REF;
    }

    /**
     * @param string $P_ORIG_SYS_DOCUMENT_REF
     * @return \edu\wisc\services\cbs\order\header\generated\InputParameters
     */
    public function setP_ORIG_SYS_DOCUMENT_REF($P_ORIG_SYS_DOCUMENT_REF)
    {
      $this->P_ORIG_SYS_DOCUMENT_REF = $P_ORIG_SYS_DOCUMENT_REF;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_CUSTOMER_TYPE()
    {
      return $this->P_CUSTOMER_TYPE;
    }

    /**
     * @param string $P_CUSTOMER_TYPE
     * @return \edu\wisc\services\cbs\order\header\generated\InputParameters
     */
    public function setP_CUSTOMER_TYPE($P_CUSTOMER_TYPE)
    {
      $this->P_CUSTOMER_TYPE = $P_CUSTOMER_TYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_CUSTOMER_IDENTIFIER()
    {
      return $this->P_CUSTOMER_IDENTIFIER;
    }

    /**
     * @param string $P_CUSTOMER_IDENTIFIER
     * @return \edu\wisc\services\cbs\order\header\generated\InputParameters
     */
    public function setP_CUSTOMER_IDENTIFIER($P_CUSTOMER_IDENTIFIER)
    {
      $this->P_CUSTOMER_IDENTIFIER = $P_CUSTOMER_IDENTIFIER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_CUSTOMER_IDENTIFIER_TYPE()
    {
      return $this->P_CUSTOMER_IDENTIFIER_TYPE;
    }

    /**
     * @param string $P_CUSTOMER_IDENTIFIER_TYPE
     * @return \edu\wisc\services\cbs\order\header\generated\InputParameters
     */
    public function setP_CUSTOMER_IDENTIFIER_TYPE($P_CUSTOMER_IDENTIFIER_TYPE)
    {
      $this->P_CUSTOMER_IDENTIFIER_TYPE = $P_CUSTOMER_IDENTIFIER_TYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_PURCHASER_IDENTIFIER()
    {
      return $this->P_PURCHASER_IDENTIFIER;
    }

    /**
     * @param string $P_PURCHASER_IDENTIFIER
     * @return \edu\wisc\services\cbs\order\header\generated\InputParameters
     */
    public function setP_PURCHASER_IDENTIFIER($P_PURCHASER_IDENTIFIER)
    {
      $this->P_PURCHASER_IDENTIFIER = $P_PURCHASER_IDENTIFIER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_PURCHASER_IDENTIFIER_TYPE()
    {
      return $this->P_PURCHASER_IDENTIFIER_TYPE;
    }

    /**
     * @param string $P_PURCHASER_IDENTIFIER_TYPE
     * @return \edu\wisc\services\cbs\order\header\generated\InputParameters
     */
    public function setP_PURCHASER_IDENTIFIER_TYPE($P_PURCHASER_IDENTIFIER_TYPE)
    {
      $this->P_PURCHASER_IDENTIFIER_TYPE = $P_PURCHASER_IDENTIFIER_TYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_CUSTOMER_REFERENCE_FIELD()
    {
      return $this->P_CUSTOMER_REFERENCE_FIELD;
    }

    /**
     * @param string $P_CUSTOMER_REFERENCE_FIELD
     * @return \edu\wisc\services\cbs\order\header\generated\InputParameters
     */
    public function setP_CUSTOMER_REFERENCE_FIELD($P_CUSTOMER_REFERENCE_FIELD)
    {
      $this->P_CUSTOMER_REFERENCE_FIELD = $P_CUSTOMER_REFERENCE_FIELD;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ORDER_SOURCE()
    {
      return $this->P_ORDER_SOURCE;
    }

    /**
     * @param string $P_ORDER_SOURCE
     * @return \edu\wisc\services\cbs\order\header\generated\InputParameters
     */
    public function setP_ORDER_SOURCE($P_ORDER_SOURCE)
    {
      $this->P_ORDER_SOURCE = $P_ORDER_SOURCE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_INTERFACE_TYPE_FLAG()
    {
      return $this->P_INTERFACE_TYPE_FLAG;
    }

    /**
     * @param string $P_INTERFACE_TYPE_FLAG
     * @return \edu\wisc\services\cbs\order\header\generated\InputParameters
     */
    public function setP_INTERFACE_TYPE_FLAG($P_INTERFACE_TYPE_FLAG)
    {
      $this->P_INTERFACE_TYPE_FLAG = $P_INTERFACE_TYPE_FLAG;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_DEFAULT_SET()
    {
      return $this->P_DEFAULT_SET;
    }

    /**
     * @param string $P_DEFAULT_SET
     * @return \edu\wisc\services\cbs\order\header\generated\InputParameters
     */
    public function setP_DEFAULT_SET($P_DEFAULT_SET)
    {
      $this->P_DEFAULT_SET = $P_DEFAULT_SET;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_TEST_MODE()
    {
      return $this->P_TEST_MODE;
    }

    /**
     * @param string $P_TEST_MODE
     * @return \edu\wisc\services\cbs\order\header\generated\InputParameters
     */
    public function setP_TEST_MODE($P_TEST_MODE)
    {
      $this->P_TEST_MODE = $P_TEST_MODE;
      return $this;
    }

}
