<?php

namespace edu\wisc\services\cbs\order\interfacing;

use edu\wisc\services\cbs\api\Service;
use edu\wisc\services\cbs\order\OrderServiceResponse;

/**
 * Service intended to "interface" orders with CBS (commit and finalize an order).
 */
interface OrderInterfacingService extends Service
{

    /**
     * Commit and finalize ("interface") an order with CBS.
     * @param float $orderNumber
     * @param string $prefix
     * @return OrderServiceResponse
     */
    public function interfaceOrder($orderNumber, $prefix = "MAGE"): OrderServiceResponse;

}
