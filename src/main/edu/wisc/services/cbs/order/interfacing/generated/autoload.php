<?php


 function autoload_fc6f4e45dc6ed3bf16dd2a84a033c550($class)
{
    $classes = array(
        'edu\wisc\services\cbs\order\interfacing\generated\DOIT_SOA_ORDER_IFACE_I_V2_Service' => __DIR__ .'/DOIT_SOA_ORDER_IFACE_I_V2_Service.php',
        'edu\wisc\services\cbs\order\interfacing\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\order\interfacing\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\order\interfacing\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_fc6f4e45dc6ed3bf16dd2a84a033c550');

// Do nothing. The rest is just leftovers from the code generation.
{
}
