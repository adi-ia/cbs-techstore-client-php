<?php
namespace edu\wisc\services\cbs\order\interfacing;

use edu\wisc\services\cbs\api\SoapService;
use edu\wisc\services\cbs\common\WssSoapHeaderBuilder;
use edu\wisc\services\cbs\order\interfacing\generated\DOIT_SOA_ORDER_IFACE_I_V2_Service;
use edu\wisc\services\cbs\order\interfacing\generated\InputParameters;
use edu\wisc\services\cbs\order\OrderServiceResponse;

/**
 * SOAP-backed implementation of {@link OrderInterfacingService}
 */
class SoapOrderInterfacingService implements OrderInterfacingService, SoapService
{

    /** URL for QA WSDL */
    const CBQA12 = 'http://pegasus.doit.wisc.edu:8018/webservices/SOAProvider/plsql/doit_soa_order_iface_i_v2/?wsdl';
    /** URL for DV WSDL */
    const CBDV12 = 'http://pegasus.doit.wisc.edu:8016/webservices/SOAProvider/plsql/doit_soa_order_iface_i_v2/?wsdl';
    /** URL for CP WSDL */
    const CBCP12 = 'http://pegasus.doit.wisc.edu:8015/webservices/SOAProvider/plsql/doit_soa_order_iface_i_v2/?wsdl';
    /** URL for PROD WSDL */
    const CBSP = 'http://galactica.doit.wisc.edu:8001/webservices/SOAProvider/plsql/doit_soa_order_iface_i_v2/?wsdl';

    /** @var DOIT_SOA_ORDER_IFACE_I_V2_Service */
    private $soapClient;

    public function __construct($username, $password, $wsdlPath = null, \SoapClient $priceSoapClient = null)
    {
        if ($priceSoapClient !== null) {
            $this->soapClient = $priceSoapClient;
            return;
        } else if ($wsdlPath !== null) {
            $this->soapClient = new DOIT_SOA_ORDER_IFACE_I_V2_Service(
                [],
                $wsdlPath
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        } else {
            $this->soapClient = new DOIT_SOA_ORDER_IFACE_I_V2_Service(
                [],
                __DIR__ . '/../../../../../../resources/doit_soa_order_iface_i_v2.xml'
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function interfaceOrder($orderNumber, $prefix = "MAGE"): OrderServiceResponse
    {
        $response = $this->soapClient->INTERFACE_ORDER(
            new InputParameters($orderNumber, $prefix . $orderNumber, 'TBD')
        );
        return new OrderServiceResponse(
            strcasecmp($response->getP_STATUS(), 'SUCCESS') === 0,
            $response->getP_RESULT_MESSAGE()
        );
    }



}
