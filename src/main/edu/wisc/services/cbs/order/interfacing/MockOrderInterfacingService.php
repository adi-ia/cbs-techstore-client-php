<?php

namespace edu\wisc\services\cbs\order\interfacing;

use edu\wisc\services\cbs\api\MockService;
use edu\wisc\services\cbs\common\ServiceResponseInterface;
use edu\wisc\services\cbs\order\OrderServiceResponse;

class MockOrderInterfacingService implements OrderInterfacingService, MockService
{

    /** @var bool */
    private $success;

    /**
     * {@inheritdoc}
     */
    public function __construct(bool $success = true)
    {
        $this->success = $success;
    }

    /**
     * {@inheritdoc}
     */
    public function interfaceOrder($orderNumber, $prefix = "MAGE"): OrderServiceResponse
    {
        return new OrderServiceResponse($this->success, "Order number: {$orderNumber}");
    }

}
