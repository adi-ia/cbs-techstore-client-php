<?php
namespace edu\wisc\services\cbs\order\line;

use edu\wisc\services\cbs\api\MockService;
use edu\wisc\services\cbs\order\OrderServiceResponse;

class MockOrderLineService implements OrderLineService, MockService
{

    /** @var OrderServiceResponse */
    private $success;

    /**
     * @inheritdoc
     */
    public function __construct(bool $success = true)
    {
        $this->success = $success;
    }

    /**
     * @inheritdoc
     */
    public function createOrderLine(OrderLine $orderLine): OrderServiceResponse
    {
        return new OrderServiceResponse($this->success, (string)$orderLine);
    }
}
