<?php

namespace edu\wisc\services\cbs\order\line\generated;

class InputParameters
{

    /**
     * @var float $P_ORDER_NUMBER
     */
    protected $P_ORDER_NUMBER = null;

    /**
     * @var string $P_ORIG_SYS_DOCUMENT_REF
     */
    protected $P_ORIG_SYS_DOCUMENT_REF = null;

    /**
     * @var float $P_LINE_NUMBER
     */
    protected $P_LINE_NUMBER = null;

    /**
     * @var string $P_ORIG_SYS_LINE_REF
     */
    protected $P_ORIG_SYS_LINE_REF = null;

    /**
     * @var string $P_ITEM_NUMBER_TYPE
     */
    protected $P_ITEM_NUMBER_TYPE = null;

    /**
     * @var float $P_ITEM_NUMBER
     */
    protected $P_ITEM_NUMBER = null;

    /**
     * @var float $P_QUANTITY
     */
    protected $P_QUANTITY = null;

    /**
     * @var float $P_UNIT_PRICE
     */
    protected $P_UNIT_PRICE = null;

    /**
     * @var float $P_UNIT_PROMO_PRICE
     */
    protected $P_UNIT_PROMO_PRICE = null;

    /**
     * @var string $P_PROMO_DISCOUNT_NAME
     */
    protected $P_PROMO_DISCOUNT_NAME = null;

    /**
     * @var float $P_BUNDLE_ID
     */
    protected $P_BUNDLE_ID = null;

    /**
     * @var float $P_BUNDLE_LINE_ID
     */
    protected $P_BUNDLE_LINE_ID = null;

    /**
     * @var string $P_DELIVERY_METHOD
     */
    protected $P_DELIVERY_METHOD = null;

    /**
     * @var \DateTime $P_SHIP_DATE
     */
    protected $P_SHIP_DATE = null;

    /**
     * @var string $P_SHIP_TO_IDENTIFIER
     */
    protected $P_SHIP_TO_IDENTIFIER = null;

    /**
     * @var string $P_SHIP_TO_IDENTIFIER_TYPE
     */
    protected $P_SHIP_TO_IDENTIFIER_TYPE = null;

    /**
     * @var float $P_SHIP_TO_ADDRESS_ID
     */
    protected $P_SHIP_TO_ADDRESS_ID = null;

    /**
     * @var string $P_SHIP_TO_ADDRESS_1
     */
    protected $P_SHIP_TO_ADDRESS_1 = null;

    /**
     * @var string $P_SHIP_TO_ADDRESS_2
     */
    protected $P_SHIP_TO_ADDRESS_2 = null;

    /**
     * @var string $P_SHIP_TO_ADDRESS_3
     */
    protected $P_SHIP_TO_ADDRESS_3 = null;

    /**
     * @var string $P_SHIP_TO_ADDRESS_4
     */
    protected $P_SHIP_TO_ADDRESS_4 = null;

    /**
     * @var string $P_SHIP_TO_CITY
     */
    protected $P_SHIP_TO_CITY = null;

    /**
     * @var string $P_SHIP_TO_STATE
     */
    protected $P_SHIP_TO_STATE = null;

    /**
     * @var string $P_SHIP_TO_POSTAL_CODE
     */
    protected $P_SHIP_TO_POSTAL_CODE = null;

    /**
     * @var string $P_SHIPPING_INSTRUCTIONS
     */
    protected $P_SHIPPING_INSTRUCTIONS = null;

    /**
     * @var float $P_SHIP_SET_ID
     */
    protected $P_SHIP_SET_ID = null;

    /**
     * @var string $P_LINE_TYPE
     */
    protected $P_LINE_TYPE = null;

    /**
     * @var float $P_USER_ID
     */
    protected $P_USER_ID = null;

    /**
     * @var string $P_TEST_MODE
     */
    protected $P_TEST_MODE = null;

    /**
     * @param float $P_ORDER_NUMBER
     * @param string $P_ORIG_SYS_DOCUMENT_REF
     * @param float $P_LINE_NUMBER
     * @param string $P_ORIG_SYS_LINE_REF
     * @param string $P_ITEM_NUMBER_TYPE
     * @param float $P_ITEM_NUMBER
     * @param float $P_QUANTITY
     * @param float $P_UNIT_PRICE
     * @param float $P_UNIT_PROMO_PRICE
     * @param string $P_PROMO_DISCOUNT_NAME
     * @param float $P_BUNDLE_ID
     * @param float $P_BUNDLE_LINE_ID
     * @param string $P_DELIVERY_METHOD
     * @param \DateTime $P_SHIP_DATE
     * @param string $P_SHIP_TO_IDENTIFIER
     * @param string $P_SHIP_TO_IDENTIFIER_TYPE
     * @param float $P_SHIP_TO_ADDRESS_ID
     * @param string $P_SHIP_TO_ADDRESS_1
     * @param string $P_SHIP_TO_ADDRESS_2
     * @param string $P_SHIP_TO_ADDRESS_3
     * @param string $P_SHIP_TO_ADDRESS_4
     * @param string $P_SHIP_TO_CITY
     * @param string $P_SHIP_TO_STATE
     * @param string $P_SHIP_TO_POSTAL_CODE
     * @param string $P_SHIPPING_INSTRUCTIONS
     * @param float $P_SHIP_SET_ID
     * @param string $P_LINE_TYPE
     * @param float $P_USER_ID
     * @param string $P_TEST_MODE
     */
    public function __construct($P_ORDER_NUMBER, $P_ORIG_SYS_DOCUMENT_REF, $P_LINE_NUMBER, $P_ORIG_SYS_LINE_REF, $P_ITEM_NUMBER_TYPE, $P_ITEM_NUMBER, $P_QUANTITY, $P_UNIT_PRICE, $P_UNIT_PROMO_PRICE, $P_PROMO_DISCOUNT_NAME, $P_BUNDLE_ID, $P_BUNDLE_LINE_ID, $P_DELIVERY_METHOD, \DateTime $P_SHIP_DATE, $P_SHIP_TO_IDENTIFIER, $P_SHIP_TO_IDENTIFIER_TYPE, $P_SHIP_TO_ADDRESS_ID, $P_SHIP_TO_ADDRESS_1, $P_SHIP_TO_ADDRESS_2, $P_SHIP_TO_ADDRESS_3, $P_SHIP_TO_ADDRESS_4, $P_SHIP_TO_CITY, $P_SHIP_TO_STATE, $P_SHIP_TO_POSTAL_CODE, $P_SHIPPING_INSTRUCTIONS, $P_SHIP_SET_ID, $P_LINE_TYPE, $P_USER_ID, $P_TEST_MODE)
    {
      $this->P_ORDER_NUMBER = $P_ORDER_NUMBER;
      $this->P_ORIG_SYS_DOCUMENT_REF = $P_ORIG_SYS_DOCUMENT_REF;
      $this->P_LINE_NUMBER = $P_LINE_NUMBER;
      $this->P_ORIG_SYS_LINE_REF = $P_ORIG_SYS_LINE_REF;
      $this->P_ITEM_NUMBER_TYPE = $P_ITEM_NUMBER_TYPE;
      $this->P_ITEM_NUMBER = $P_ITEM_NUMBER;
      $this->P_QUANTITY = $P_QUANTITY;
      $this->P_UNIT_PRICE = $P_UNIT_PRICE;
      $this->P_UNIT_PROMO_PRICE = $P_UNIT_PROMO_PRICE;
      $this->P_PROMO_DISCOUNT_NAME = $P_PROMO_DISCOUNT_NAME;
      $this->P_BUNDLE_ID = $P_BUNDLE_ID;
      $this->P_BUNDLE_LINE_ID = $P_BUNDLE_LINE_ID;
      $this->P_DELIVERY_METHOD = $P_DELIVERY_METHOD;
      $this->P_SHIP_DATE = $P_SHIP_DATE->format(\DateTime::ATOM);
      $this->P_SHIP_TO_IDENTIFIER = $P_SHIP_TO_IDENTIFIER;
      $this->P_SHIP_TO_IDENTIFIER_TYPE = $P_SHIP_TO_IDENTIFIER_TYPE;
      $this->P_SHIP_TO_ADDRESS_ID = $P_SHIP_TO_ADDRESS_ID;
      $this->P_SHIP_TO_ADDRESS_1 = $P_SHIP_TO_ADDRESS_1;
      $this->P_SHIP_TO_ADDRESS_2 = $P_SHIP_TO_ADDRESS_2;
      $this->P_SHIP_TO_ADDRESS_3 = $P_SHIP_TO_ADDRESS_3;
      $this->P_SHIP_TO_ADDRESS_4 = $P_SHIP_TO_ADDRESS_4;
      $this->P_SHIP_TO_CITY = $P_SHIP_TO_CITY;
      $this->P_SHIP_TO_STATE = $P_SHIP_TO_STATE;
      $this->P_SHIP_TO_POSTAL_CODE = $P_SHIP_TO_POSTAL_CODE;
      $this->P_SHIPPING_INSTRUCTIONS = $P_SHIPPING_INSTRUCTIONS;
      $this->P_SHIP_SET_ID = $P_SHIP_SET_ID;
      $this->P_LINE_TYPE = $P_LINE_TYPE;
      $this->P_USER_ID = $P_USER_ID;
      $this->P_TEST_MODE = $P_TEST_MODE;
    }

    /**
     * @return float
     */
    public function getP_ORDER_NUMBER()
    {
      return $this->P_ORDER_NUMBER;
    }

    /**
     * @param float $P_ORDER_NUMBER
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_ORDER_NUMBER($P_ORDER_NUMBER)
    {
      $this->P_ORDER_NUMBER = $P_ORDER_NUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ORIG_SYS_DOCUMENT_REF()
    {
      return $this->P_ORIG_SYS_DOCUMENT_REF;
    }

    /**
     * @param string $P_ORIG_SYS_DOCUMENT_REF
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_ORIG_SYS_DOCUMENT_REF($P_ORIG_SYS_DOCUMENT_REF)
    {
      $this->P_ORIG_SYS_DOCUMENT_REF = $P_ORIG_SYS_DOCUMENT_REF;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_LINE_NUMBER()
    {
      return $this->P_LINE_NUMBER;
    }

    /**
     * @param float $P_LINE_NUMBER
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_LINE_NUMBER($P_LINE_NUMBER)
    {
      $this->P_LINE_NUMBER = $P_LINE_NUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ORIG_SYS_LINE_REF()
    {
      return $this->P_ORIG_SYS_LINE_REF;
    }

    /**
     * @param string $P_ORIG_SYS_LINE_REF
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_ORIG_SYS_LINE_REF($P_ORIG_SYS_LINE_REF)
    {
      $this->P_ORIG_SYS_LINE_REF = $P_ORIG_SYS_LINE_REF;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ITEM_NUMBER_TYPE()
    {
      return $this->P_ITEM_NUMBER_TYPE;
    }

    /**
     * @param string $P_ITEM_NUMBER_TYPE
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_ITEM_NUMBER_TYPE($P_ITEM_NUMBER_TYPE)
    {
      $this->P_ITEM_NUMBER_TYPE = $P_ITEM_NUMBER_TYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_ITEM_NUMBER()
    {
      return $this->P_ITEM_NUMBER;
    }

    /**
     * @param float $P_ITEM_NUMBER
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_ITEM_NUMBER($P_ITEM_NUMBER)
    {
      $this->P_ITEM_NUMBER = $P_ITEM_NUMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_QUANTITY()
    {
      return $this->P_QUANTITY;
    }

    /**
     * @param float $P_QUANTITY
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_QUANTITY($P_QUANTITY)
    {
      $this->P_QUANTITY = $P_QUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_UNIT_PRICE()
    {
      return $this->P_UNIT_PRICE;
    }

    /**
     * @param float $P_UNIT_PRICE
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_UNIT_PRICE($P_UNIT_PRICE)
    {
      $this->P_UNIT_PRICE = $P_UNIT_PRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_UNIT_PROMO_PRICE()
    {
      return $this->P_UNIT_PROMO_PRICE;
    }

    /**
     * @param float $P_UNIT_PROMO_PRICE
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_UNIT_PROMO_PRICE($P_UNIT_PROMO_PRICE)
    {
      $this->P_UNIT_PROMO_PRICE = $P_UNIT_PROMO_PRICE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_PROMO_DISCOUNT_NAME()
    {
      return $this->P_PROMO_DISCOUNT_NAME;
    }

    /**
     * @param string $P_PROMO_DISCOUNT_NAME
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_PROMO_DISCOUNT_NAME($P_PROMO_DISCOUNT_NAME)
    {
      $this->P_PROMO_DISCOUNT_NAME = $P_PROMO_DISCOUNT_NAME;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_BUNDLE_ID()
    {
      return $this->P_BUNDLE_ID;
    }

    /**
     * @param float $P_BUNDLE_ID
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_BUNDLE_ID($P_BUNDLE_ID)
    {
      $this->P_BUNDLE_ID = $P_BUNDLE_ID;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_BUNDLE_LINE_ID()
    {
      return $this->P_BUNDLE_LINE_ID;
    }

    /**
     * @param float $P_BUNDLE_LINE_ID
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_BUNDLE_LINE_ID($P_BUNDLE_LINE_ID)
    {
      $this->P_BUNDLE_LINE_ID = $P_BUNDLE_LINE_ID;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_DELIVERY_METHOD()
    {
      return $this->P_DELIVERY_METHOD;
    }

    /**
     * @param string $P_DELIVERY_METHOD
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_DELIVERY_METHOD($P_DELIVERY_METHOD)
    {
      $this->P_DELIVERY_METHOD = $P_DELIVERY_METHOD;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getP_SHIP_DATE()
    {
      if ($this->P_SHIP_DATE == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->P_SHIP_DATE);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $P_SHIP_DATE
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIP_DATE(\DateTime $P_SHIP_DATE)
    {
      $this->P_SHIP_DATE = $P_SHIP_DATE->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SHIP_TO_IDENTIFIER()
    {
      return $this->P_SHIP_TO_IDENTIFIER;
    }

    /**
     * @param string $P_SHIP_TO_IDENTIFIER
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIP_TO_IDENTIFIER($P_SHIP_TO_IDENTIFIER)
    {
      $this->P_SHIP_TO_IDENTIFIER = $P_SHIP_TO_IDENTIFIER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SHIP_TO_IDENTIFIER_TYPE()
    {
      return $this->P_SHIP_TO_IDENTIFIER_TYPE;
    }

    /**
     * @param string $P_SHIP_TO_IDENTIFIER_TYPE
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIP_TO_IDENTIFIER_TYPE($P_SHIP_TO_IDENTIFIER_TYPE)
    {
      $this->P_SHIP_TO_IDENTIFIER_TYPE = $P_SHIP_TO_IDENTIFIER_TYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_SHIP_TO_ADDRESS_ID()
    {
      return $this->P_SHIP_TO_ADDRESS_ID;
    }

    /**
     * @param float $P_SHIP_TO_ADDRESS_ID
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIP_TO_ADDRESS_ID($P_SHIP_TO_ADDRESS_ID)
    {
      $this->P_SHIP_TO_ADDRESS_ID = $P_SHIP_TO_ADDRESS_ID;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SHIP_TO_ADDRESS_1()
    {
      return $this->P_SHIP_TO_ADDRESS_1;
    }

    /**
     * @param string $P_SHIP_TO_ADDRESS_1
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIP_TO_ADDRESS_1($P_SHIP_TO_ADDRESS_1)
    {
      $this->P_SHIP_TO_ADDRESS_1 = $P_SHIP_TO_ADDRESS_1;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SHIP_TO_ADDRESS_2()
    {
      return $this->P_SHIP_TO_ADDRESS_2;
    }

    /**
     * @param string $P_SHIP_TO_ADDRESS_2
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIP_TO_ADDRESS_2($P_SHIP_TO_ADDRESS_2)
    {
      $this->P_SHIP_TO_ADDRESS_2 = $P_SHIP_TO_ADDRESS_2;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SHIP_TO_ADDRESS_3()
    {
      return $this->P_SHIP_TO_ADDRESS_3;
    }

    /**
     * @param string $P_SHIP_TO_ADDRESS_3
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIP_TO_ADDRESS_3($P_SHIP_TO_ADDRESS_3)
    {
      $this->P_SHIP_TO_ADDRESS_3 = $P_SHIP_TO_ADDRESS_3;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SHIP_TO_ADDRESS_4()
    {
      return $this->P_SHIP_TO_ADDRESS_4;
    }

    /**
     * @param string $P_SHIP_TO_ADDRESS_4
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIP_TO_ADDRESS_4($P_SHIP_TO_ADDRESS_4)
    {
      $this->P_SHIP_TO_ADDRESS_4 = $P_SHIP_TO_ADDRESS_4;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SHIP_TO_CITY()
    {
      return $this->P_SHIP_TO_CITY;
    }

    /**
     * @param string $P_SHIP_TO_CITY
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIP_TO_CITY($P_SHIP_TO_CITY)
    {
      $this->P_SHIP_TO_CITY = $P_SHIP_TO_CITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SHIP_TO_STATE()
    {
      return $this->P_SHIP_TO_STATE;
    }

    /**
     * @param string $P_SHIP_TO_STATE
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIP_TO_STATE($P_SHIP_TO_STATE)
    {
      $this->P_SHIP_TO_STATE = $P_SHIP_TO_STATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SHIP_TO_POSTAL_CODE()
    {
      return $this->P_SHIP_TO_POSTAL_CODE;
    }

    /**
     * @param string $P_SHIP_TO_POSTAL_CODE
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIP_TO_POSTAL_CODE($P_SHIP_TO_POSTAL_CODE)
    {
      $this->P_SHIP_TO_POSTAL_CODE = $P_SHIP_TO_POSTAL_CODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SHIPPING_INSTRUCTIONS()
    {
      return $this->P_SHIPPING_INSTRUCTIONS;
    }

    /**
     * @param string $P_SHIPPING_INSTRUCTIONS
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIPPING_INSTRUCTIONS($P_SHIPPING_INSTRUCTIONS)
    {
      $this->P_SHIPPING_INSTRUCTIONS = $P_SHIPPING_INSTRUCTIONS;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_SHIP_SET_ID()
    {
      return $this->P_SHIP_SET_ID;
    }

    /**
     * @param float $P_SHIP_SET_ID
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_SHIP_SET_ID($P_SHIP_SET_ID)
    {
      $this->P_SHIP_SET_ID = $P_SHIP_SET_ID;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_LINE_TYPE()
    {
      return $this->P_LINE_TYPE;
    }

    /**
     * @param string $P_LINE_TYPE
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_LINE_TYPE($P_LINE_TYPE)
    {
      $this->P_LINE_TYPE = $P_LINE_TYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_USER_ID()
    {
      return $this->P_USER_ID;
    }

    /**
     * @param float $P_USER_ID
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_USER_ID($P_USER_ID)
    {
      $this->P_USER_ID = $P_USER_ID;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_TEST_MODE()
    {
      return $this->P_TEST_MODE;
    }

    /**
     * @param string $P_TEST_MODE
     * @return \edu\wisc\services\cbs\order\line\generated\InputParameters
     */
    public function setP_TEST_MODE($P_TEST_MODE)
    {
      $this->P_TEST_MODE = $P_TEST_MODE;
      return $this;
    }

}
