<?php

namespace edu\wisc\services\cbs\order\payment\generated;

class OutputParameters
{

    /**
     * @var string $P_STATUS
     */
    protected $P_STATUS = null;

    /**
     * @var string $P_RESULT_MESSAGE
     */
    protected $P_RESULT_MESSAGE = null;

    /**
     * @var float $P_CHANGE_BACK
     */
    protected $P_CHANGE_BACK = null;

    /**
     * @var float $P_BALANCE_DUE
     */
    protected $P_BALANCE_DUE = null;

    /**
     * @var float $P_PAID_TO_DATE
     */
    protected $P_PAID_TO_DATE = null;

    /**
     * @param string $P_STATUS
     * @param string $P_RESULT_MESSAGE
     * @param float $P_CHANGE_BACK
     * @param float $P_BALANCE_DUE
     * @param float $P_PAID_TO_DATE
     */
    public function __construct($P_STATUS, $P_RESULT_MESSAGE, $P_CHANGE_BACK, $P_BALANCE_DUE, $P_PAID_TO_DATE)
    {
      $this->P_STATUS = $P_STATUS;
      $this->P_RESULT_MESSAGE = $P_RESULT_MESSAGE;
      $this->P_CHANGE_BACK = $P_CHANGE_BACK;
      $this->P_BALANCE_DUE = $P_BALANCE_DUE;
      $this->P_PAID_TO_DATE = $P_PAID_TO_DATE;
    }

    /**
     * @return string
     */
    public function getP_STATUS()
    {
      return $this->P_STATUS;
    }

    /**
     * @param string $P_STATUS
     * @return \edu\wisc\services\cbs\order\payment\generated\OutputParameters
     */
    public function setP_STATUS($P_STATUS)
    {
      $this->P_STATUS = $P_STATUS;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_RESULT_MESSAGE()
    {
      return $this->P_RESULT_MESSAGE;
    }

    /**
     * @param string $P_RESULT_MESSAGE
     * @return \edu\wisc\services\cbs\order\payment\generated\OutputParameters
     */
    public function setP_RESULT_MESSAGE($P_RESULT_MESSAGE)
    {
      $this->P_RESULT_MESSAGE = $P_RESULT_MESSAGE;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_CHANGE_BACK()
    {
      return $this->P_CHANGE_BACK;
    }

    /**
     * @param float $P_CHANGE_BACK
     * @return \edu\wisc\services\cbs\order\payment\generated\OutputParameters
     */
    public function setP_CHANGE_BACK($P_CHANGE_BACK)
    {
      $this->P_CHANGE_BACK = $P_CHANGE_BACK;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_BALANCE_DUE()
    {
      return $this->P_BALANCE_DUE;
    }

    /**
     * @param float $P_BALANCE_DUE
     * @return \edu\wisc\services\cbs\order\payment\generated\OutputParameters
     */
    public function setP_BALANCE_DUE($P_BALANCE_DUE)
    {
      $this->P_BALANCE_DUE = $P_BALANCE_DUE;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_PAID_TO_DATE()
    {
      return $this->P_PAID_TO_DATE;
    }

    /**
     * @param float $P_PAID_TO_DATE
     * @return \edu\wisc\services\cbs\order\payment\generated\OutputParameters
     */
    public function setP_PAID_TO_DATE($P_PAID_TO_DATE)
    {
      $this->P_PAID_TO_DATE = $P_PAID_TO_DATE;
      return $this;
    }

}
