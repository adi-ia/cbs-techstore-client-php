<?php


 function autoload_8d57331e741d60c809fd7513ebff3374($class)
{
    $classes = array(
        'edu\wisc\services\cbs\order\payment\generated\DOIT_SOA_PAYMENT_V2_Service' => __DIR__ .'/DOIT_SOA_PAYMENT_V2_Service.php',
        'edu\wisc\services\cbs\order\payment\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\order\payment\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\order\payment\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_8d57331e741d60c809fd7513ebff3374');

// Do nothing. The rest is just leftovers from the code generation.
{
}
