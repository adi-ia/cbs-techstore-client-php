<?php

namespace edu\wisc\services\cbs\order\payment\generated;

class InputParameters
{

    /**
     * @var float $P_ORDER_NUMBER
     */
    protected $P_ORDER_NUMBER = null;

    /**
     * @var string $P_ORIG_SYS_DOCUMENT_REF
     */
    protected $P_ORIG_SYS_DOCUMENT_REF = null;

    /**
     * @var string $P_PAYMENT_TYPE
     */
    protected $P_PAYMENT_TYPE = null;

    /**
     * @var string $P_PAYMENT_INFO
     */
    protected $P_PAYMENT_INFO = null;

    /**
     * @var string $P_CASH_REGISTER
     */
    protected $P_CASH_REGISTER = null;

    /**
     * @var float $P_RENDERD_AMOUNT
     */
    protected $P_RENDERD_AMOUNT = null;

    /**
     * @var float $P_SUBTOTAL
     */
    protected $P_SUBTOTAL = null;

    /**
     * @var float $P_TAX
     */
    protected $P_TAX = null;

    /**
     * @var float $P_TOTAL
     */
    protected $P_TOTAL = null;

    /**
     * @var string $P_PAYMENT_NOTES
     */
    protected $P_PAYMENT_NOTES = null;

    /**
     * @var string $P_SALES_REP
     */
    protected $P_SALES_REP = null;

    /**
     * @var string $P_ORDER_SOURCE
     */
    protected $P_ORDER_SOURCE = null;

    /**
     * @var \DateTime $P_PAYMENT_DATE
     */
    protected $P_PAYMENT_DATE = null;

    /**
     * @var string $P_TEST_MODE
     */
    protected $P_TEST_MODE = null;

    /**
     * @param float $P_ORDER_NUMBER
     * @param string $P_ORIG_SYS_DOCUMENT_REF
     * @param string $P_PAYMENT_TYPE
     * @param string $P_PAYMENT_INFO
     * @param string $P_CASH_REGISTER
     * @param float $P_RENDERD_AMOUNT
     * @param float $P_SUBTOTAL
     * @param float $P_TAX
     * @param float $P_TOTAL
     * @param string $P_PAYMENT_NOTES
     * @param string $P_SALES_REP
     * @param string $P_ORDER_SOURCE
     * @param \DateTime $P_PAYMENT_DATE
     * @param string $P_TEST_MODE
     */
    public function __construct($P_ORDER_NUMBER, $P_ORIG_SYS_DOCUMENT_REF, $P_PAYMENT_TYPE, $P_PAYMENT_INFO, $P_CASH_REGISTER, $P_RENDERD_AMOUNT, $P_SUBTOTAL, $P_TAX, $P_TOTAL, $P_PAYMENT_NOTES, $P_SALES_REP, $P_ORDER_SOURCE, \DateTime $P_PAYMENT_DATE, $P_TEST_MODE)
    {
      $this->P_ORDER_NUMBER = $P_ORDER_NUMBER;
      $this->P_ORIG_SYS_DOCUMENT_REF = $P_ORIG_SYS_DOCUMENT_REF;
      $this->P_PAYMENT_TYPE = $P_PAYMENT_TYPE;
      $this->P_PAYMENT_INFO = $P_PAYMENT_INFO;
      $this->P_CASH_REGISTER = $P_CASH_REGISTER;
      $this->P_RENDERD_AMOUNT = $P_RENDERD_AMOUNT;
      $this->P_SUBTOTAL = $P_SUBTOTAL;
      $this->P_TAX = $P_TAX;
      $this->P_TOTAL = $P_TOTAL;
      $this->P_PAYMENT_NOTES = $P_PAYMENT_NOTES;
      $this->P_SALES_REP = $P_SALES_REP;
      $this->P_ORDER_SOURCE = $P_ORDER_SOURCE;
      $this->P_PAYMENT_DATE = $P_PAYMENT_DATE->format(\DateTime::ATOM);
      $this->P_TEST_MODE = $P_TEST_MODE;
    }

    /**
     * @return float
     */
    public function getP_ORDER_NUMBER()
    {
      return $this->P_ORDER_NUMBER;
    }

    /**
     * @param float $P_ORDER_NUMBER
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_ORDER_NUMBER($P_ORDER_NUMBER)
    {
      $this->P_ORDER_NUMBER = $P_ORDER_NUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ORIG_SYS_DOCUMENT_REF()
    {
      return $this->P_ORIG_SYS_DOCUMENT_REF;
    }

    /**
     * @param string $P_ORIG_SYS_DOCUMENT_REF
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_ORIG_SYS_DOCUMENT_REF($P_ORIG_SYS_DOCUMENT_REF)
    {
      $this->P_ORIG_SYS_DOCUMENT_REF = $P_ORIG_SYS_DOCUMENT_REF;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_PAYMENT_TYPE()
    {
      return $this->P_PAYMENT_TYPE;
    }

    /**
     * @param string $P_PAYMENT_TYPE
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_PAYMENT_TYPE($P_PAYMENT_TYPE)
    {
      $this->P_PAYMENT_TYPE = $P_PAYMENT_TYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_PAYMENT_INFO()
    {
      return $this->P_PAYMENT_INFO;
    }

    /**
     * @param string $P_PAYMENT_INFO
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_PAYMENT_INFO($P_PAYMENT_INFO)
    {
      $this->P_PAYMENT_INFO = $P_PAYMENT_INFO;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_CASH_REGISTER()
    {
      return $this->P_CASH_REGISTER;
    }

    /**
     * @param string $P_CASH_REGISTER
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_CASH_REGISTER($P_CASH_REGISTER)
    {
      $this->P_CASH_REGISTER = $P_CASH_REGISTER;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_RENDERD_AMOUNT()
    {
      return $this->P_RENDERD_AMOUNT;
    }

    /**
     * @param float $P_RENDERD_AMOUNT
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_RENDERD_AMOUNT($P_RENDERD_AMOUNT)
    {
      $this->P_RENDERD_AMOUNT = $P_RENDERD_AMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_SUBTOTAL()
    {
      return $this->P_SUBTOTAL;
    }

    /**
     * @param float $P_SUBTOTAL
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_SUBTOTAL($P_SUBTOTAL)
    {
      $this->P_SUBTOTAL = $P_SUBTOTAL;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_TAX()
    {
      return $this->P_TAX;
    }

    /**
     * @param float $P_TAX
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_TAX($P_TAX)
    {
      $this->P_TAX = $P_TAX;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_TOTAL()
    {
      return $this->P_TOTAL;
    }

    /**
     * @param float $P_TOTAL
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_TOTAL($P_TOTAL)
    {
      $this->P_TOTAL = $P_TOTAL;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_PAYMENT_NOTES()
    {
      return $this->P_PAYMENT_NOTES;
    }

    /**
     * @param string $P_PAYMENT_NOTES
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_PAYMENT_NOTES($P_PAYMENT_NOTES)
    {
      $this->P_PAYMENT_NOTES = $P_PAYMENT_NOTES;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SALES_REP()
    {
      return $this->P_SALES_REP;
    }

    /**
     * @param string $P_SALES_REP
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_SALES_REP($P_SALES_REP)
    {
      $this->P_SALES_REP = $P_SALES_REP;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ORDER_SOURCE()
    {
      return $this->P_ORDER_SOURCE;
    }

    /**
     * @param string $P_ORDER_SOURCE
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_ORDER_SOURCE($P_ORDER_SOURCE)
    {
      $this->P_ORDER_SOURCE = $P_ORDER_SOURCE;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getP_PAYMENT_DATE()
    {
      if ($this->P_PAYMENT_DATE == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->P_PAYMENT_DATE);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $P_PAYMENT_DATE
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_PAYMENT_DATE(\DateTime $P_PAYMENT_DATE)
    {
      $this->P_PAYMENT_DATE = $P_PAYMENT_DATE->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getP_TEST_MODE()
    {
      return $this->P_TEST_MODE;
    }

    /**
     * @param string $P_TEST_MODE
     * @return \edu\wisc\services\cbs\order\payment\generated\InputParameters
     */
    public function setP_TEST_MODE($P_TEST_MODE)
    {
      $this->P_TEST_MODE = $P_TEST_MODE;
      return $this;
    }

}
