<?php

namespace edu\wisc\services\cbs\order\payment\generated;

class DOIT_SOA_PAYMENT_V2_Service extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'InputParameters' => 'edu\\wisc\\services\\cbs\\order\\payment\\generated\\InputParameters',
      'OutputParameters' => 'edu\\wisc\\services\\cbs\\order\\payment\\generated\\OutputParameters',
      'SOAHeader' => 'edu\\wisc\\services\\cbs\\order\\payment\\generated\\SOAHeader',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'src/main/resources/doit_soa_payment_v2.xml';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param InputParameters $body
     * @return OutputParameters
     */
    public function PAYMENT_CREATE(InputParameters $body)
    {
      return $this->__soapCall('PAYMENT_CREATE', array($body));
    }

}
