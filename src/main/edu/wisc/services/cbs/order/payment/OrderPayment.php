<?php

namespace edu\wisc\services\cbs\order\payment;

use edu\wisc\services\cbs\common\Order;

/**
 * OrderPayment represents payment information needed by CBS for processing an order.
 */
class OrderPayment extends Order
{

    /** @var float */
    private $orderNumber;

    /** @var string */
    private $origSysDocumentRef;

    /** @var string Approval Code (check #, CC approval) */
    private $paymentInfo;

    /** @var string Credit Card or DNumber */
    private $paymentType;

    /** @var string IP of register used for sale */
    private $register;

    /** @var float Amount of payment */
    private $renderedAmount;

    /** @var float */
    private $subtotal;

    /** @var float */
    private $tax;

    /** @var float */
    private $total;

    /** @var string (optional) */
    private $paymentNotes;

    /** @var string Fixed for Magento orders */
    private $salesRep = Order::SALES_REP;

    /** @var string Fixed for Magento orders */
    private $orderSource = Order::ORDER_SOURCE;

    /** @var string */
    private $testMode;

    /**
     * @return float
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param float $orderNumber
     * @return OrderPayment
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrigSysDocumentRef()
    {
        return $this->origSysDocumentRef;
    }

    /**
     * @param string $origSysDocumentRef
     * @return OrderPayment
     */
    public function setOrigSysDocumentRef($origSysDocumentRef)
    {
        $this->origSysDocumentRef = $origSysDocumentRef;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentInfo()
    {
        return $this->paymentInfo;
    }

    /**
     * @param string $paymentInfo
     * @return OrderPayment
     */
    public function setPaymentInfo($paymentInfo)
    {
        $this->paymentInfo = $paymentInfo;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param string $paymentType
     * @return OrderPayment
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegister()
    {
        return $this->register;
    }

    /**
     * @param string $register
     * @return OrderPayment
     */
    public function setRegister($register)
    {
        $this->register = $register;
        return $this;
    }

    /**
     * @return float
     */
    public function getRenderedAmount()
    {
        return $this->renderedAmount;
    }

    /**
     * @param float $renderedAmount
     * @return OrderPayment
     */
    public function setRenderedAmount($renderedAmount)
    {
        $this->renderedAmount = $renderedAmount;
        return $this;
    }

    /**
     * @return float
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param float $subtotal
     * @return OrderPayment
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
        return $this;
    }

    /**
     * @return float
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param float $tax
     * @return OrderPayment
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     * @return OrderPayment
     */
    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentNotes()
    {
        return $this->paymentNotes;
    }

    /**
     * @param string $paymentNotes
     * @return OrderPayment
     */
    public function setPaymentNotes(string $paymentNotes = null): OrderPayment
    {
        $this->paymentNotes = $paymentNotes;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalesRep(): string
    {
        return $this->salesRep;
    }

    /**
     * @param string $salesRep
     * @return OrderPayment
     */
    public function setSalesRep($salesRep)
    {
        $this->salesRep = $salesRep;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderSource()
    {
        return $this->orderSource;
    }

    /**
     * @param string $orderSource
     * @return OrderPayment
     */
    public function setOrderSource($orderSource)
    {
        $this->orderSource = $orderSource;
        return $this;
    }

    /**
     * @return string
     */
    public function getTestMode()
    {
        return $this->testMode;
    }

    /**
     * @param string $testMode
     * @return OrderPayment
     */
    public function setTestMode($testMode)
    {
        $this->testMode = $testMode;
        return $this;
    }

}