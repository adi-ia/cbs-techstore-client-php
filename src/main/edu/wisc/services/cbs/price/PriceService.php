<?php

namespace edu\wisc\services\cbs\price;

use edu\wisc\services\cbs\api\Service;

/**
 * Service intended for managing product prices.
 */
interface PriceService extends Service
{
    /**
     * @param Price $price
     * @return PriceServiceResponse
     */
    public function updateProductPrice(Price $price): PriceServiceResponse;

}
