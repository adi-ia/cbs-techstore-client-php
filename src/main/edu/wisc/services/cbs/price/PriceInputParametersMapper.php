<?php

namespace edu\wisc\services\cbs\price;

use edu\wisc\services\cbs\price\generated\InputParameters;

/**
 * Class PriceInputParametersMapper
 * @package edu\wisc\services\cbs\price
 */
class PriceInputParametersMapper
{

    /**
     * @param Price $price
     * @return InputParameters
     */
    public static function toInputParameters(Price $price)
    {
        return (new InputParameters(
            '',
            0.0,
            '',
            '',
            ''
        ))
            ->setP_ITEM_NUMBER($price->getItemNumber())
            ->setP_GLOBAL_PRICE($price->getGlobalPrice())
            ->setP_WEBSITE_PRICING($price->getWebsitePricing())
            ->setP_REFERENCE($price->getReference())
            ->setP_TEST_MODE($price->getTestMode());
    }
}