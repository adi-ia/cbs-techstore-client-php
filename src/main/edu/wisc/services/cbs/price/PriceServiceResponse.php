<?php

namespace edu\wisc\services\cbs\price;

use edu\wisc\services\cbs\common\AbstractServiceResponse;

/**
 * The status of a call to a {@link ProductPriceService} call.
 */
class PriceServiceResponse extends AbstractServiceResponse
{

}
