<?php

namespace edu\wisc\services\cbs\price\generated;

class DOIT_SOA_PRICING_V2_Service extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'InputParameters' => 'edu\\wisc\\services\\cbs\\price\\generated\\InputParameters',
      'OutputParameters' => 'edu\\wisc\\services\\cbs\\price\\generated\\OutputParameters',
      'SOAHeader' => 'edu\\wisc\\services\\cbs\\price\\generated\\SOAHeader',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'src/main/resources/doit_soa_pricing_v2.xml';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param InputParameters $body
     * @return OutputParameters
     */
    public function ITEM_PRICING(InputParameters $body)
    {
      return $this->__soapCall('ITEM_PRICING', array($body));
    }

}
