<?php


 function autoload_0206368e05ead65395e6a2c400991a7e($class)
{
    $classes = array(
        'edu\wisc\services\cbs\price\generated\DOIT_SOA_PRICING_V2_Service' => __DIR__ .'/DOIT_SOA_PRICING_V2_Service.php',
        'edu\wisc\services\cbs\price\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\price\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\price\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_0206368e05ead65395e6a2c400991a7e');

// Do nothing. The rest is just leftovers from the code generation.
{
}
