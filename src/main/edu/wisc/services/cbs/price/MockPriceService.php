<?php

namespace edu\wisc\services\cbs\price;

use edu\wisc\services\cbs\api\MockService;

/**
 * Mock {@link PriceService} implementation
 */
class MockPriceService implements PriceService, MockService
{

    /** @var bool */
    private $success;

    /**
     * @inheritdoc
     */
    public function __construct(bool $success = true)
    {
        $this->success = $success;
    }

    /**
     * {@inheritdoc}
     */
    public function updateProductPrice(Price $price): PriceServiceResponse
    {
        return new PriceServiceResponse($this->success, "SKU {$price->getItemNumber()}: {$price->getGlobalPrice()}");
    }
}
