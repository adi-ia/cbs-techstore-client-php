<?php

namespace edu\wisc\services\cbs\price;


class Price
{
    /**
     * @var string
     */
    private $itemNumber;

    /**
     * @var float
     */
    private $globalPrice;

    /**
     * @var string
     */
    private $websitePricing;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var string
     */
    private $testMode;

    /**
     * @return string
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * @param string $itemNumber
     * @return Price
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;
        return $this;
    }

    /**
     * @return float
     */
    public function getGlobalPrice()
    {
        return $this->globalPrice;
    }

    /**
     * @param float $globalPrice
     * @return Price
     */
    public function setGlobalPrice($globalPrice)
    {
        $this->globalPrice = $globalPrice;
        return $this;
    }

    /**
     * @return string (json format)
     */
    public function getWebsitePricing()
    {
        return $this->websitePricing;
    }

    /**
     * @param string $websitePricing (json format)
     * @return Price
     */
    public function setWebsitePricing($websitePricing)
    {
        $this->websitePricing = $websitePricing;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return Price
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return string
     */
    public function getTestMode()
    {
        return $this->testMode;
    }

    /**
     * @param string $testMode
     * @return Price
     */
    public function setTestMode($testMode)
    {
        $this->testMode = $testMode;
        return $this;
    }


}