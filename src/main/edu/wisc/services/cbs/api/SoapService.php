<?php
namespace edu\wisc\services\cbs\api;

/**
 * A contract that CBS SOAP clients must follow to ensure consistency during instantiation.
 */
interface SoapService extends Service
{

    /**
     * A SOAP service is constructed with a username and password. If a {@link \SoapClient} is provided, the username
     * and password are ignored.
     *
     * @param string $username  username
     * @param string $password  password
     * @param string|null $wsdlPath path to WSDL for specifying CBS environment. Defaults to QA
     * @param \SoapClient|null $soapClient  Overriding SOAP client (for testing purposes)
     */
    public function __construct($username, $password, $wsdlPath = null, \SoapClient $soapClient = null);
}
