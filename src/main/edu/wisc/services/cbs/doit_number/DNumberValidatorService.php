<?php

namespace edu\wisc\services\cbs\doit_number;

/**
 * DNumberValidatorService validates that a given customer can make purchases with a particular
 * DoIT Number.
 */
interface DNumberValidatorService
{

    /**
     * @param string $dnumber
     * @param string $customerType
     * @param string $customerIdentifier
     * @param string $customerIdentifierType
     * @return DNumberValidatorServiceResponse
     */
    public function validateDNumber($dnumber, $customerType, $customerIdentifier, $customerIdentifierType);

}