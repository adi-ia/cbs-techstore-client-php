<?php

namespace edu\wisc\services\cbs\doit_number;

/**
 * Mock implementation of {@link DNumberValidatorService}.
 */
class MockDNumberValidatorService implements DNumberValidatorService
{

    /** @var DNumberValidatorServiceResponse */
    private $response;

    public function __construct($response = null)
    {
        if ($response !== null) {
            $this->response = $response;
        } else {
            $this->response = new DNumberValidatorServiceResponse(
                true,
                "Mock DNumberValidatorService Response"
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validateDNumber($dnumber, $customerType, $customerIdentifier, $customerIdentifierType)
    {
        return $this->response;
    }

}