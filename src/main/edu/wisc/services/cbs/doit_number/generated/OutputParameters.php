<?php

namespace edu\wisc\services\cbs\doit_number\generated;

class OutputParameters
{

    /**
     * @var string $P_RESULT_STRING
     */
    protected $P_RESULT_STRING = null;

    /**
     * @param string $P_RESULT_STRING
     */
    public function __construct($P_RESULT_STRING)
    {
      $this->P_RESULT_STRING = $P_RESULT_STRING;
    }

    /**
     * @return string
     */
    public function getP_RESULT_STRING()
    {
      return $this->P_RESULT_STRING;
    }

    /**
     * @param string $P_RESULT_STRING
     * @return \edu\wisc\services\cbs\doit_number\generated\OutputParameters
     */
    public function setP_RESULT_STRING($P_RESULT_STRING)
    {
      $this->P_RESULT_STRING = $P_RESULT_STRING;
      return $this;
    }

}
