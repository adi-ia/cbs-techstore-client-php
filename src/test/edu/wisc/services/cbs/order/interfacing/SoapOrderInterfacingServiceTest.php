<?php
namespace edu\wisc\services\cbs\order\interfacing;

use edu\wisc\services\cbs\order\interfacing\generated\DOIT_SOA_ORDER_IFACE_I_V2_Service;
use edu\wisc\services\cbs\order\interfacing\generated\InputParameters;
use edu\wisc\services\cbs\order\interfacing\generated\OutputParameters;
use edu\wisc\services\cbs\order\OrderServiceResponse;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;

/**
 * Tests for {@link SoapOrderInterfacingService}
 */
class SoapOrderInterfacingServiceTest extends TestCase
{

    /** @var MockInterface */
    private $mockSoapClient;

    /** @var OrderInterfacingService */
    private $orderInterfacingService;

    /** Create fresh mock before each test */
    protected function setUp()
    {
        $this->mockSoapClient = \Mockery::mock(DOIT_SOA_ORDER_IFACE_I_V2_Service::class);
        $this->orderInterfacingService = new SoapOrderInterfacingService('', '', null, $this->mockSoapClient);
    }

    /** Close/verify mock after each test */
    protected function tearDown()
    {
        \Mockery::close();
    }

    /** @test Verifies 'MAGE' is prepended to order number */
    public function generatesCorrectInputParameters()
    {
        $inputParameters = new InputParameters('1234', 'MAGE1234', 'TBD');
        $this->mockSoapClient->shouldReceive('INTERFACE_ORDER')->with(
            \Mockery::on(
                function ($arg) use ($inputParameters) {
                    return $arg == $inputParameters;
                }
            )
        )->andReturn(new OutputParameters('SUCCESS', 'Order interfaced'))->once();
        static::assertEquals(
            new OrderServiceResponse(true, 'Order interfaced'),
            $this->orderInterfacingService->interfaceOrder('1234')
        );
    }

    /** @test */
    public function handlesUnsuccessfulResponse()
    {
        $this->mockSoapClient->shouldReceive('INTERFACE_ORDER')->andReturn(new OutputParameters('ERROR', 'Oh jeezus'));
        static::assertEquals(
            new OrderServiceResponse(false, 'Oh jeezus'),
            $this->orderInterfacingService->interfaceOrder('1234')
        );
    }
}
