<?php

use edu\wisc\services\cbs\order\payment\MockOrderPaymentService;
use edu\wisc\services\cbs\order\payment\OrderPayment;
use edu\wisc\services\cbs\order\payment\OrderPaymentServiceResponse;
use Money\Money;
use PHPUnit\Framework\TestCase;

class MockOrderPaymentServiceTest extends TestCase
{

    /** @test */
    public function alwaysSucceedsByDefault()
    {
        $client = new MockOrderPaymentService();
        $payment = new OrderPayment();
        $response = $client->createOrderPayment($payment);
        static::assertTrue($response->isSuccess());
    }

}
