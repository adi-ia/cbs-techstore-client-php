<?php

use edu\wisc\services\cbs\order\line\MockOrderLineService;
use edu\wisc\services\cbs\order\line\OrderLine;
use edu\wisc\services\cbs\order\OrderServiceResponse;

use PHPUnit\Framework\TestCase;

/**
 * Tests for {@link MockOrderLineService}
 */
class MockOrderLineServiceTest extends TestCase
{

    /** @test */
    public function alwaysSucceedsByDefault()
    {
        $line = new OrderLine();
        $service = new MockOrderLineService();
        $response = $service->createOrderLine($line);
        static::assertTrue($response->isSuccess());
    }

}
