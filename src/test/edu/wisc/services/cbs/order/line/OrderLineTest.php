<?php
namespace edu\wisc\services\cbs\order\line;

use PHPUnit\Framework\TestCase;

class OrderLineTest extends TestCase
{
    public function testToArray()
    {
        $orderLine = new OrderLine();
        $array = $orderLine->toArray();
        static::assertNotCount(0, $array);
        static::assertArrayHasKey('itemNumberType', $array);
        static::assertEquals('item_number', $array['itemNumberType']);
    }
}
