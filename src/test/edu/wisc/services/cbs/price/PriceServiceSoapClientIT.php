<?php
namespace edu\wisc\services\cbs\price;

use edu\wisc\services\cbs\product\Product;
use edu\wisc\services\cbs\IntegrationTestCase;
use edu\wisc\services\cbs\product\ProductService;
use edu\wisc\services\cbs\product\SoapProductService;

class PriceServiceSoapClientIT extends IntegrationTestCase
{
    /** @var  SoapPriceService */
    private $client;

    /** @var Product */
    private $product;

    /** @var ProductService */
    private $productService;

    /** @var Price */
    private $price;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->client = new SoapPriceService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
        $this->productService = new SoapProductService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
        $this->product = $this->generateSampleProduct();
        $this->product->setItemNumber("CBSPRICESVCSOAP");
        $this->product->setDescription("CBS Price Service SOAP Client IT");
        $this->productService->createProduct($this->product);

        $this->price = (new Price())
            ->setItemNumber('75083')
            ->setGlobalPrice(9.90)
            ->setWebsitePricing('{ "student": 88.88 }')
            ->setReference('whatever')
            ->setTestMode('Yes');
    }

    /** @test */
    public function updatesProductPrice()
    {
        $response = $this->client->updateProductPrice($this->price);
        static::assertTrue($response->isSuccess(), $response->getMessage());
    }
}
