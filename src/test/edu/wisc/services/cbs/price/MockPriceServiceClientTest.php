<?php

namespace edu\wisc\services\cbs\price;

use PHPUnit\Framework\TestCase;

class MockPriceServiceClientTest extends TestCase
{

    /**
     * @var Price
     */
    private $price;

    protected function setUp()
    {
        $this->price = (new Price())
            ->setItemNumber('75083')
            ->setGlobalPrice(9.90)
            ->setWebsitePricing('{ "student": 88.88 }')
            ->setReference('whatever')
            ->setTestMode('Yes');
    }

    /** @test */
    public function alwaysSucceedsByDefault()
    {
        $client = new MockPriceService();
        $response = $client->updateProductPrice($this->price);
        static::assertTrue($response->isSuccess());
    }
    
    /** @test */
    public function returnsFailureResponse()
    {
        $client = new MockPriceService(false);
        $response = $client->updateProductPrice($this->price);
        static::assertFalse($response->isSuccess());
    }

}

