<?php
namespace edu\wisc\services\cbs;

use edu\wisc\services\cbs\common\Identifiers;
use edu\wisc\services\cbs\common\Order;
use edu\wisc\services\cbs\common\Shipping;
use edu\wisc\services\cbs\order\header\OrderHeader;
use edu\wisc\services\cbs\order\line\OrderLine;
use edu\wisc\services\cbs\order\payment\OrderPayment;
use edu\wisc\services\cbs\product\Product;
use Money\Money;
use PHPUnit\Framework\TestCase;

/**
 * A base test class for integration tests.
 *
 * <p>Loads the integration test data located in `src/test/resources/it-configuration.ini`</p>
 */
class IntegrationTestCase extends TestCase
{

    /** @var array  integration test data */
    protected static $itData;

    /**
     * Reads the integration test data
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        $itDataPath = __DIR__ . '/../../../../resources/it-configuration.ini';
        static::$itData = parse_ini_file($itDataPath);
        if (static::$itData === false) {
            static::fail("Unable to read integration test data from $itDataPath");
        }
    }

    /**
     * Generate a sample product for integration tests.
     * @return Product
     */
    protected function generateSampleProduct()
    {
        $itemNumber = mt_rand(0, 99999);
        $cost = mt_rand(1, 1000000) / 100;
        return (new Product())
            ->setItemNumber('85000')
            ->setDescription('ADI IA Test Product')
            ->setCbsItemTemplate('Commodity')
            ->setItemCategory('COMP/APPLE/DESKTOP')
            ->setManufacturer('Hoffmann Manufacturers')
            ->setManufacturerPartNumber("HOFF$itemNumber")
            ->setCost($cost)
            ->setMinQuantity(1)
            ->setMaxQuantity(5)
            ->setLifecycle('SELLABLE_ONLY')
            ->setInventoryControlType('STOCK')
            ->setSerialControlFlag('None')
            ->setInventoryControlType('STOCK')
            ->setVendor('B&H Photo')
            ->setVendorPartNumber("BLAIR$itemNumber")
            ->setUpc(uniqid('', false))
            ->setBasePrice(5.0)
            ->setPricingTemplate(1)
            ->setReference('ref')
            ->setCostCenter('5056')
            ->setPlannerCode('PM-JAM')
            ->setMinMaxActive('no')
            ->setBuyer('NONE')
            ->setProductManager('Chris Last')
            ->setTestMode('yesplz');
    }

    /**
     * Generate a sample {@link OrderHeader} for integration tests.
     * @param $orderNumber
     * @return OrderHeader
     */
    protected function generateSampleOrderHeader($orderNumber)
    {
        return (new OrderHeader())
            ->setOrderNumber($orderNumber)
            ->setOrigSystemDocumentRef(Order::ORIG_SYS_PREFIX . $orderNumber)
            ->setCustomerType(Identifiers::PERSONAL)
            ->setCustomerIdentifier('UW027S180')
            ->setCustomerIdentifierType(Identifiers::PVI)
            ->setPurchaserIdentifier('UW027S180')
            ->setPurchaserIdentifierType(Identifiers::PVI)
            ->setCustomerReferenceField('TBD')
            ->setInterfaceTypeFlag('TBD')
            ->setDefaultSet('TBD')
            ->setTestMode('yesplz');
    }

    /**
     * Generate a sample {@link OrderLine} for integration tests.
     * @param $orderNumber
     * @return OrderLine
     */
    protected function generateSampleOrderLine($orderNumber)
    {
        $product = $this->generateSampleProduct();
        $lineNumber = 1;
        return (new OrderLine())
            ->setOrderNumber($orderNumber)
            ->setOrigSysDocumentRef("MAGE" . $orderNumber)
            ->setLineNumber($lineNumber)
            ->setOrigSysLineRef(Order::ORIG_SYS_PREFIX . $orderNumber . '-' . $lineNumber)
            ->setItemNumber($product->getItemNumber())
            ->setQuantity(1)
            ->setUnitPrice(1.000)
            ->setUnitPromoPrice(0.0)
            ->setPromoDiscountName('none')
            ->setDeliveryMethod(Shipping::UPS_SHIPPING)
            ->setShipToIdentifier('UW027S180')
            ->setShipToIdentifierType(Identifiers::PVI)
            ->setShipToAddress1('123 N. South St. Madison, WI 53703')
            ->setShipToCity('Madison')
            ->setShipToState('WI')
            ->setShipToPostalCode('53703')
            ->setUserId(6.0)
            ->setTestMode('yesplz');
    }

    /**
     * Generate a sample {@link OrderPayment} for integration tests.
     * @param $orderNumber
     * @return OrderPayment
     */
    protected function generateSampleOrderPayment($orderNumber)
    {
        return (new OrderPayment())
            ->setOrderNumber($orderNumber)
            ->setOrigSysDocumentRef("MAGE" . $orderNumber)
            ->setPaymentInfo('1234')
            ->setPaymentType('CREDIT CARD')
            ->setRegister('0.0.0.0')
            ->setRenderedAmount(10.00)
            ->setSubtotal(9.950)
            ->setTax(0.05)
            ->setTotal(10.00)
            ->setTestMode('yesplz');
    }

}
