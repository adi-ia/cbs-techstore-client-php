<?php
namespace edu\wisc\services\cbs\doit_number;

use PHPUnit\Framework\TestCase;

/**
 * Tests for {@link MockDNumberValidatorService}.
 */
class MockDNumberValidatorServiceTest extends TestCase
{

    /**
     * @test
     */
    public function alwaysSucceedsByDefault()
    {
        $client = new MockDNumberValidatorService();
        $response = $client->validateDNumber("D000123", "DEPARTMENTAL", "UW123A456", "PVI");
        static::assertTrue($response->isSuccess());
    }

    /**
     * @test
     */
    public function returnsCustomResponse()
    {
        $mockResponse = new DNumberValidatorServiceResponse(true, "Custom DNumber Validator Service Response");
        $client = new MockDNumberValidatorService($mockResponse);
        $response = $client->validateDNumber("D000123", "DEPARTMENTAL", "UW123A456", "PVI");
        static::assertEquals($mockResponse, $response);
    }

}
