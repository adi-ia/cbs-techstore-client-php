<?php
namespace edu\wisc\services\cbs\product;

use edu\wisc\services\cbs\product\create\generated\InputParameters;
use PHPUnit\Framework\TestCase;

/**
 * Unit tests for {@link ProductInputParametersMapper}
 */
class ProductInputParametersMapperTest extends TestCase
{

    /** @test */
    public function mapsProductToCreateInputParameters()
    {
        $product = $this->createFixture(99);

        $inputParameters = ProductInputParametersMapper::toInputParameters(
            $product,
            ProductInputParametersMapper::$CREATE
        );

        static::assertInstanceOf(InputParameters::class, $inputParameters);
        static::assertEquals($product->getAddSalesInstructions(), $inputParameters->getP_ADD_SALES_INSTRUCTIONS());
        static::assertEquals($product->getAttributeSet(), $inputParameters->getP_ATTRIBUTE_SET());
        static::assertEquals($product->getBasePrice(), $inputParameters->getP_BASE_PRICE());
        static::assertEquals($product->getBuyer(), $inputParameters->getP_BUYER());
        static::assertEquals($product->getCbsItemTemplate(), $inputParameters->getP_CBS_ITEM_TEMPLATE());
        static::assertEquals($product->getCost(), $inputParameters->getP_COST());
        static::assertEquals($product->getCostCenter(), $inputParameters->getP_COST_CENTER());
        static::assertEquals($product->getDepEligible(), $inputParameters->getP_DEP_ELIGIBLE());
        static::assertEquals($product->getDescription(), $inputParameters->getP_ITEM_DESCRIPTION());
        static::assertEquals($product->getEcmMax(), $inputParameters->getP_ECM_MAX());
        static::assertEquals($product->getEcmMin(), $inputParameters->getP_ECM_MIN());
        static::assertEquals($product->getEcmPrimaryLocator(), $inputParameters->getP_ECM_PRIMARY_LOCATOR());
        static::assertEquals($product->getEcmSecondaryLocator(), $inputParameters->getP_ECM_SECONDARY_LOCATOR());
        static::assertEquals($product->getFixedLotMultiplier(), $inputParameters->getP_FIXED_LOT_MULTIPLIER());
        static::assertEquals($product->getHslcMax(), $inputParameters->getP_HSLC_MAX());
        static::assertEquals($product->getHslcMin(), $inputParameters->getP_HSLC_MIN());
        static::assertEquals($product->getHslcPrimaryLocator(), $inputParameters->getP_HSLC_PRIMARY_LOCATOR());
        static::assertEquals($product->getHslcSecondaryLocator(), $inputParameters->getP_HSLC_SECONDARY_LOCATOR());
        static::assertEquals($product->getInventoryControlType(), $inputParameters->getP_INVENTORY_CONTROL_TYPE());
        static::assertEquals($product->getItemCategory(), $inputParameters->getP_ITEM_CATEGORY());
        static::assertEquals($product->getItemNumber(), $inputParameters->getP_ITEM_NUMBER());
        static::assertEquals($product->getLifecycle(), $inputParameters->getP_LIFE_CYCLE());
        static::assertEquals($product->getManufacturer(), $inputParameters->getP_MANUFACTURER_NAME());
        static::assertEquals($product->getManufacturerPartNumber(), $inputParameters->getP_MANUFACTURER_PART_NUMBER());
        static::assertEquals($product->getMaxQuantity(), $inputParameters->getP_MINMAX_LEVEL_MAX());
        static::assertEquals($product->getMinMaxActive(), $inputParameters->getP_MINMAX_ACTIVE());
        static::assertEquals($product->getMinQuantity(), $inputParameters->getP_MINMAX_LEVEL_MIN());
        static::assertEquals($product->getPlannerCode(), $inputParameters->getP_PLANNER_CODE());
        static::assertEquals($product->getPricingTemplate(), $inputParameters->getP_PRICING_TEMPLATE());
        static::assertEquals($product->getProductManager(), $inputParameters->getP_PRODUCT_MANAGER());
        static::assertEquals($product->getPsMax(), $inputParameters->getP_PS_MAX());
        static::assertEquals($product->getPsMin(), $inputParameters->getP_PS_MIN());
        static::assertEquals($product->getPsPrimaryLocator(), $inputParameters->getP_PS_PRIMARY_LOCATOR());
        static::assertEquals($product->getPsSecondaryLocator(), $inputParameters->getP_PS_SECONDARY_LOCATOR());
        static::assertEquals($product->getReference(), $inputParameters->getP_REFERENCE());
        static::assertEquals($product->getSalesPoint(), $inputParameters->getP_SALES_POINT());
        static::assertEquals($product->getSalesProcedure(), $inputParameters->getP_SALES_PROCEDURE());
        static::assertEquals($product->getSerialControlFlag(), $inputParameters->getP_SERIAL_CONTROL_FLAG());
        static::assertEquals($product->getSerialEndString(), $inputParameters->getP_SERIAL_END_STRING());
        static::assertEquals($product->getSerialLengthMax(), $inputParameters->getP_SERIAL_LENGTH_MAX());
        static::assertEquals($product->getSerialLengthMin(), $inputParameters->getP_SERIAL_LENGTH_MIN());
        static::assertEquals($product->getSerialStartString(), $inputParameters->getP_SERIAL_START_STRING());
        static::assertEquals($product->getSerialStartString(), $inputParameters->getP_SERIAL_START_STRING());
        static::assertEquals($product->getTaxClassId(), $inputParameters->getP_TAX_CLASS_ID());
        static::assertEquals($product->getTestMode(), $inputParameters->getP_TEST_MODE());
        static::assertEquals($product->getUpc(), $inputParameters->getP_UPC());
        static::assertEquals($product->getUpcAlternate(), $inputParameters->getP_UPC_ALTERNATE());
        static::assertEquals($product->getVendor(), $inputParameters->getP_VENDOR());
        static::assertEquals($product->getVendorPartNumber(), $inputParameters->getP_SUPPLIER_PART_NUMBER());
        static::assertEquals($product->getWebsitePricing(), $inputParameters->getP_WEBSITE_PRICING());
    }

    /** @test */
    public function mapsProductToUpdateParameters()
    {
        $product = $this->createFixture(99);

        $inputParameters = ProductInputParametersMapper::toInputParameters(
            $product,
            ProductInputParametersMapper::$UPDATE
        );
        static::assertInstanceOf(\edu\wisc\services\cbs\product\update\generated\InputParameters::class, $inputParameters);
        static::assertEquals($product->getAddSalesInstructions(), $inputParameters->getP_ADD_SALES_INSTRUCTIONS());
        static::assertEquals($product->getAttributeSet(), $inputParameters->getP_ATTRIBUTE_SET());
        static::assertEquals($product->getBasePrice(), $inputParameters->getP_BASE_PRICE());
        static::assertEquals($product->getBuyer(), $inputParameters->getP_BUYER());
        static::assertEquals($product->getCbsItemTemplate(), $inputParameters->getP_CBS_ITEM_TEMPLATE());
        static::assertEquals($product->getCost(), $inputParameters->getP_COST());
        static::assertEquals($product->getCostCenter(), $inputParameters->getP_COST_CENTER());
        static::assertEquals($product->getDepEligible(), $inputParameters->getP_DEP_ELIGIBLE());
        static::assertEquals($product->getDescription(), $inputParameters->getP_ITEM_DESCRIPTION());
        static::assertEquals($product->getEcmMax(), $inputParameters->getP_ECM_MAX());
        static::assertEquals($product->getEcmMin(), $inputParameters->getP_ECM_MIN());
        static::assertEquals($product->getEcmPrimaryLocator(), $inputParameters->getP_ECM_PRIMARY_LOCATOR());
        static::assertEquals($product->getEcmSecondaryLocator(), $inputParameters->getP_ECM_SECONDARY_LOCATOR());
        static::assertEquals($product->getFixedLotMultiplier(), $inputParameters->getP_FIXED_LOT_MULTIPLIER());
        static::assertEquals($product->getHslcMax(), $inputParameters->getP_HSLC_MAX());
        static::assertEquals($product->getHslcMin(), $inputParameters->getP_HSLC_MIN());
        static::assertEquals($product->getHslcPrimaryLocator(), $inputParameters->getP_HSLC_PRIMARY_LOCATOR());
        static::assertEquals($product->getHslcSecondaryLocator(), $inputParameters->getP_HSLC_SECONDARY_LOCATOR());
        static::assertEquals($product->getInventoryControlType(), $inputParameters->getP_INVENTORY_CONTROL_TYPE());
        static::assertEquals($product->getItemCategory(), $inputParameters->getP_ITEM_CATEGORY());
        static::assertEquals($product->getItemNumber(), $inputParameters->getP_ITEM_NUMBER());
        static::assertEquals($product->getLifecycle(), $inputParameters->getP_LIFE_CYCLE());
        static::assertEquals($product->getManufacturer(), $inputParameters->getP_MANUFACTURER_NAME());
        static::assertEquals($product->getManufacturerPartNumber(), $inputParameters->getP_MANUFACTURER_PART_NUMBER());
        static::assertEquals($product->getMaxQuantity(), $inputParameters->getP_MINMAX_LEVEL_MAX());
        static::assertEquals($product->getMinMaxActive(), $inputParameters->getP_MINMAX_ACTIVE());
        static::assertEquals($product->getMinQuantity(), $inputParameters->getP_MINMAX_LEVEL_MIN());
        static::assertEquals($product->getPlannerCode(), $inputParameters->getP_PLANNER_CODE());
        static::assertEquals($product->getPricingTemplate(), $inputParameters->getP_PRICING_TEMPLATE());
        static::assertEquals($product->getProductManager(), $inputParameters->getP_PRODUCT_MANAGER());
        static::assertEquals($product->getPsMax(), $inputParameters->getP_PS_MAX());
        static::assertEquals($product->getPsMin(), $inputParameters->getP_PS_MIN());
        static::assertEquals($product->getPsPrimaryLocator(), $inputParameters->getP_PS_PRIMARY_LOCATOR());
        static::assertEquals($product->getPsSecondaryLocator(), $inputParameters->getP_PS_SECONDARY_LOCATOR());
        static::assertEquals($product->getReference(), $inputParameters->getP_REFERENCE());
        static::assertEquals($product->getSalesPoint(), $inputParameters->getP_SALES_POINT());
        static::assertEquals($product->getSalesProcedure(), $inputParameters->getP_SALES_PROCEDURE());
        static::assertEquals($product->getSerialControlFlag(), $inputParameters->getP_SERIAL_CONTROL_FLAG());
        static::assertEquals($product->getSerialEndString(), $inputParameters->getP_SERIAL_END_STRING());
        static::assertEquals($product->getSerialLengthMax(), $inputParameters->getP_SERIAL_LENGTH_MAX());
        static::assertEquals($product->getSerialLengthMin(), $inputParameters->getP_SERIAL_LENGTH_MIN());
        static::assertEquals($product->getSerialStartString(), $inputParameters->getP_SERIAL_START_STRING());
        static::assertEquals($product->getSerialStartString(), $inputParameters->getP_SERIAL_START_STRING());
        static::assertEquals($product->getTaxClassId(), $inputParameters->getP_TAX_CLASS_ID());
        static::assertEquals($product->getTestMode(), $inputParameters->getP_TEST_MODE());
        static::assertEquals($product->getUpc(), $inputParameters->getP_UPC());
        static::assertEquals($product->getUpcAlternate(), $inputParameters->getP_UPC_ALTERNATE());
        static::assertEquals($product->getVendor(), $inputParameters->getP_VENDOR());
        static::assertEquals($product->getVendorPartNumber(), $inputParameters->getP_SUPPLIER_PART_NUMBER());
        static::assertEquals($product->getWebsitePricing(), $inputParameters->getP_WEBSITE_PRICING());

    }

    /**
     * Creates a sample product with given cost
     *
     * @param int $cost  cost in US cents
     * @return Product
     */
    private function createFixture(int $cost = null): Product
    {
        $product = (new Product())
            ->setAddSalesInstructions('Special sales instructions go here')
            ->setAttributeSet('Default Attribute Set')
            ->setBasePrice(50)
            ->setBuyer('APPLEBUYER')
            ->setCbsItemTemplate('Commodity')
            ->setCostCenter(5060)
            ->setDepEligible(1)
            ->setDescription('Item description')
            ->setEcmMax(382)
            ->setEcmMin(95)
            ->setEcmPrimaryLocator('EC.01')
            ->setEcmSecondaryLocator('EC.02')
            ->setFixedLotMultiplier(10)
            ->setHslcMax(700)
            ->setHslcMin(655)
            ->setHslcPrimaryLocator('HS.01')
            ->setHslcSecondaryLocator('HS.02')
            ->setInventoryControlType('STOCK')
            ->setItemCategory('HARD/MP3Players/EARBUDS')
            ->setItemNumber('75083')
            ->setLifecycle('SELLABLE_ONLY')
            ->setManufacturer('Apple')
            ->setManufacturerPartNumber('MNHF2AM/A')
            ->setMaxQuantity(26)
            ->setMinMaxActive('yes')
            ->setMinQuantity(3)
            ->setPlannerCode('APPLE-MM')
            ->setPricingTemplate('1')
            ->setProductManager('Dawn Karls')
            ->setPsMax(769)
            ->setPsMin(620)
            ->setPsPrimaryLocator('PS.01')
            ->setPsSecondaryLocator('PS.02')
            ->setReference('reference')
            ->setSalesPoint('TECHSTORE')
            ->setSalesProcedure('Sales procedure')
            ->setSerialControlFlag('None')
            ->setSerialEndString('serial end string')
            ->setSerialLengthMax(753)
            ->setSerialLengthMin(431)
            ->setSerialStartString('serial start string')
            ->setTaxClassId('Taxable Goods')
            ->setTestMode('yesplz')
            ->setUpc('1901919819819')
            ->setUpcAlternate('237727987298373')
            ->setVendor('vendor')
            ->setVendorPartNumber('MNHF2AM/A-VENDOR')
            ->setWebsitePricing('{"departmental: 9.99}');

        if ($cost !== null) {
            $product->setCost($cost);
        }

        return $product;
    }
}
