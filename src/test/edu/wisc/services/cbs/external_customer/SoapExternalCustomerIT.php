<?php

use edu\wisc\services\cbs\IntegrationTestCase;
use edu\wisc\services\cbs\external_customer\SoapExternalCustomerService;

class SoapExternalCustomerIT extends IntegrationTestCase
{

    /**
     * @test
     */
    public function createsClientWithOnlyUsernameAndPassword()
    {
        $soapExternalCustomerService = new SoapExternalCustomerService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
        static::assertNotNull($soapExternalCustomerService);
    }

    /**
     * @test
     */
    public function createsClientWithWsdl()
    {
        $soapExternalCustomerService = new SoapExternalCustomerService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password'],
            SoapExternalCustomerService::CBQA12
        );
        static::assertNotNull($soapExternalCustomerService);
    }

}