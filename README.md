# PHP Client for the CBS API

A PHP client for the CBS APIs exposed for the UW-Madison DoIT Techstore.

## Getting Started

### Requirements

* PHP >= 7
* PHP [SOAP Extension](http://php.net/manual/en/book.soap.php)

### Installing with composer

You can use  [composer](https://getcomposer.org/) to include this client in your project. Add the following to your
`composer.json` file:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "git@git.doit.wisc.edu:adi-ia/cbs-techstore-client-php.git"
    }
  ]
}
```

You can now include **adi-ia/cbs-techstore-client-php** to the `require` section of your `composer.json` file.

To use the client, your application must require the `vendor/autoload.php` file that Composer creates.

### Sending a request

```php
<?php
use edu\wisc\services\cbs\api\Product;
use edu\wisc\services\cbs\client\ProductServiceSoapClient;
use Money\Money;

$client = new ProductServiceSoapClient('username', 'password');
$product = (new Product())
    ->setItemNumber('itemNumber')
    ->setName('name')
    ->setCost(Money::USD(999)); // $9.99
$success = $client->createProduct($product)->isSuccess();
```

Please review the PHPDocs for these classes for more information.

### Mock client

A mock client `edu\wisc\services\cbs\client\MockProductServiceClient` is provided, which always returns that a product
was successfully created. It can also be configured to always return that a product was not created by passing a
constructor argument. See the class's PHPDoc for details.

## Releasing the Client

You must have write access to the upstream repository to cut a release. This guide assumes you have an *upstream*
remote set up in your Git repository.

1. Determine the version number you want to release. This project uses [semantic versioning](http://semver.org/). In
most cases, this should be the version currently in `composer.json`.
2. Run `git pull upstream master` to ensure you are cutting a release from the latest changes.
3. Verify the *version* field in `composer.json` and `sonar-runner.properties` is correct and matches your release.
4. Run `composer update`.
5. Commit/push any version number changes to `composer.json` and `composer.lock` to the upstream master branch. (No PR
required.)
6. Run `./vendor/bin/phing release -Drelease.version=x.x.x` where *x.x.x* is the version you want to release
7. Increment the *version* fields in `composer.json` and `sonar-runner.properties` by one patch version.
8. Commit and push changes to the upstream master branch. (No PR required.)

A tag will be created in the upstream repository for the given version. It will be immediately available to composer
users.

## References

* [CBS Homepage](https://financial.doit.wisc.edu/cbs/cbshome.asp)
* CBS assistance: [cbs-support@lists.wisc.edu](mailto:cbs-support@lists.wisc.edu)
* [Techstore Project](https://git.doit.wisc.edu/adi-ia/techstore)
